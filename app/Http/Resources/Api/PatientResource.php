<?php

namespace App\Http\Resources\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $arr = [];
        $arr['name']=$this->name;
        $arr['email']=$this->email;
        $arr['mobile']=$this->mobile;
        $arr['gender']=$this->gender_str;
        $arr['nationality_id']=($this->normal_user&&$this->normal_user->nationality)?$this->normal_user->nationality->final_name:'-';
        $arr['address']=($this->normal_user)?$this->normal_user->address:'-';
        $arr['identity']=($this->normal_user)?$this->normal_user->identity:'-';
        $arr['insurance_id']=($this->normal_user&&$this->normal_user->insurance)?$this->normal_user->insurance->final_name:'-';
        $arr['dob']=($this->normal_user)?$this->normal_user->dob:'-';
        $arr['medical_status']=($this->normal_user)?$this->normal_user->medical_status:'-';
        return $arr;
    }
}
