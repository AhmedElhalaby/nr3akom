<?php

namespace App\Http\Controllers\Admin\AppUser;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\AppUser\Doctor\UpdatePasswordRequest;
use App\Http\Requests\Admin\AppUser\Doctor\ActivationRequest;
use App\Http\Requests\Admin\AppUser\Doctor\DestroyRequest;
use App\Http\Requests\Admin\AppUser\Doctor\ExportRequest;
use App\Http\Requests\Admin\AppUser\Doctor\SearchRequest;
use App\Http\Requests\Admin\AppUser\Doctor\StoreRequest;
use App\Http\Requests\Admin\AppUser\Doctor\UpdateRequest;
use App\Master;
use App\Models\Auth\User;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class DoctorController extends Controller
{
    protected $view_index = 'admin.app_users.doctors.index';
    protected $view_show = 'admin.app_users.doctors.show';
    protected $view_create = 'admin.app_users.doctors.create';
    protected $view_edit = 'admin.app_users.doctors.edit';
    protected $view_export = 'admin.exports.doctors';
    protected $Params = [
        'Names'=>'Doctor.doctors',
        'TheName'=>'Doctor.the_doctor',
        'Name'=>'Doctor.doctor',
        'redirect'=>'admin/app_users/doctors',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = User::where('id',$id)->where('type',User::TYPES['Doctor'])->first();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = User::where('id',$id)->where('type',User::TYPES['Doctor'])->first();
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }
    /**
     * Update the Password resource in storage.
     *
     * @param UpdatePasswordRequest $request
     * @return UpdatePasswordRequest
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id)
    {
        return  $request->preset($this->Params['redirect']);
    }
}
