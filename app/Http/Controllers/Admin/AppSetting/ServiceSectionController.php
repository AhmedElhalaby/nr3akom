<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\AppSetting\ServiceSection\ActivationRequest;
use App\Http\Requests\Admin\AppSetting\ServiceSection\DestroyRequest;
use App\Http\Requests\Admin\AppSetting\ServiceSection\ExportRequest;
use App\Http\Requests\Admin\AppSetting\ServiceSection\SearchRequest;
use App\Http\Requests\Admin\AppSetting\ServiceSection\StoreRequest;
use App\Http\Requests\Admin\AppSetting\ServiceSection\UpdateRequest;
use App\Master;
use App\Models\General\ServiceSection;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class ServiceSectionController extends Controller
{
    protected $view_index = 'admin.app_settings.service_sections.index';
    protected $view_show = 'admin.app_settings.service_sections.show';
    protected $view_create = 'admin.app_settings.service_sections.create';
    protected $view_edit = 'admin.app_settings.service_sections.edit';
    protected $view_export = 'admin.exports.service_sections';
    protected $Params = [
        'Names'=>'ServiceSection.service_sections',
        'TheName'=>'ServiceSection.the_service_section',
        'Name'=>'ServiceSection.service_section',
        'redirect'=>'admin/app_settings/services',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @param $id
     * @return Factory|View
     */
    public function index(SearchRequest $request,$id)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return Factory|View
     */
    public function create($id)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return view($this->view_create)->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @param $id
     * @return Response
     */
    public function store(StoreRequest $request,$id)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @param $id2
     * @return Response
     */
    public function show($id,$id2)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        $Object = ServiceSection::find($id2);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param $id2
     * @return Response
     */
    public function edit($id,$id2)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        $Object = ServiceSection::find($id2);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @param $id2
     * @return Response
     */
    public function update(UpdateRequest $request, $id,$id2)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return $request->preset($this->Params['redirect'],$id2);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @param $id
     * @return Response
     */
    public function destroy(DestroyRequest $request,$id)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @param $id
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request,$id)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return  $request->preset($this->view_export,$this->Params,$id);
    }

    /**
     * @param ActivationRequest $request
     * @param $id
     * @param $id2
     * @return ActivationRequest
     */
    public function activation(ActivationRequest $request, $id,$id2)
    {
        $this->Params['redirect'] = 'admin/app_settings/services/'.$id.'/service_sections';
        return  $request->preset($this->Params['redirect']);
    }
}
