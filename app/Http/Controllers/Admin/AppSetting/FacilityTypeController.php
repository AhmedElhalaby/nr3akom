<?php

namespace App\Http\Controllers\Admin\AppSetting;

use App\Http\Controllers\Admin\Controller;
use App\Http\Requests\Admin\AppSetting\FacilityType\DestroyRequest;
use App\Http\Requests\Admin\AppSetting\FacilityType\ExportRequest;
use App\Http\Requests\Admin\AppSetting\FacilityType\SearchRequest;
use App\Http\Requests\Admin\AppSetting\FacilityType\StoreRequest;
use App\Http\Requests\Admin\AppSetting\FacilityType\UpdateRequest;
use App\Master;
use App\Models\General\FacilityType;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FacilityTypeController extends Controller
{
    protected $view_index = 'admin.app_settings.facility_types.index';
    protected $view_show = 'admin.app_settings.facility_types.show';
    protected $view_create = 'admin.app_settings.facility_types.create';
    protected $view_edit = 'admin.app_settings.facility_types.edit';
    protected $view_export = 'admin.exports.facility_types';
    protected $Params = [
        'Names'=>'FacilityType.facility_types',
        'TheName'=>'FacilityType.the_facility_type',
        'Name'=>'FacilityType.facility_type',
        'redirect'=>'admin/app_settings/facility_types',
    ];

    /**
     * Display a listing of the resource.
     *
     * @param SearchRequest $request
     * @return Factory|View
     */
    public function index(SearchRequest $request)
    {
        return $request->preset($this->view_index,$this->Params);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view($this->view_create)->with($this->Params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest $request
     * @return Response
     */
    public function store(StoreRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $Object = FacilityType::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_show,compact('Object'))->with($this->Params);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $Object = FacilityType::find($id);
        if(!$Object)
            return redirect($this->Params['redirect'])->withErrors(__('admin.messages.wrong_data'));
        return view($this->view_edit,compact('Object'))->with($this->Params);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Response
     */
    public function update(UpdateRequest $request, $id)
    {
        return $request->preset($this->Params['redirect'],$id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param DestroyRequest $request
     * @return Response
     */
    public function destroy(DestroyRequest $request)
    {
        return $request->preset($this->Params['redirect']);
    }

    /**
     * Export resource in storage.
     *
     * @param ExportRequest $request
     * @return Master|Factory|View|BinaryFileResponse
     */
    public function export(ExportRequest $request)
    {
        return  $request->preset($this->view_export,$this->Params);
    }

}
