<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Auth\Admin;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function index(){
        return view('welcome');
    }
    public function privacy(){
        return view('privacy');
    }
    public function lang(){
        if(session('my_locale','en') =='en'){
            session(['my_locale' => 'ar']);
        }else{
            session(['my_locale' => 'en']);
        }
        App::setLocale(session('my_locale'));
        return redirect()->back();
    }
    public function test(){
        return dd(DB::table('facilities')
            ->join('facility_services', function ($join) {
                $join->on('facilities.id', '=', 'facility_services.facility_id')
                    ->where('facility_services.service_section_id', '=', 1);
            })
            ->get());
    }
}
