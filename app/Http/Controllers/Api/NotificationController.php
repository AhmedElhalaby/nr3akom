<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\General\Notification;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    use ResponseTrait;

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request){
        $Notifications = Notification::where('user_id',auth()->user()->id)->paginate($request->has('per_page')?$request->per_page:10);
        return $this->successJsonResponse([],$Notifications->items(),'Notifications',$Notifications);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function read($id){
        $Notification = Notification::where('user_id',auth()->user()->id)->where('id',$id)->first();
        if($Notification){
            $Notification->update(array('read_at'=>now()));
            return $this->successJsonResponse([__('messages.updated_successful')],$Notification,'Notification');
        }
        return $this->failJsonResponse([__('messages.object_not_found')]);
    }

    /**
     * @return JsonResponse
     */
    public function read_all(){
        $Notification = Notification::where('user_id',auth()->user()->id)->where('read_at',null)->update(array('read_at'=>now()));
        return $this->successJsonResponse([__('messages.updated_successful')]);
    }

}
