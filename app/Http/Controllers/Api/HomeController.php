<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Master;
use App\Models\Auth\User;
use App\Models\General\DepartmentType;
use App\Models\General\FacilityType;
use App\Models\General\Insurance;
use App\Models\General\Nationality;
use App\Models\General\Service;
use App\Models\General\Specialization;
use App\Models\Orders\Order;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @return JsonResponse
     */
    public function install(){
        $DepartmentType = DepartmentType::all();
        $Insurance = Insurance::all();
        $Services = Service::all();
        $FacilityType= FacilityType::all();
        $Specialization= Specialization::all();
        $Nationality= Nationality::all();

        return $this->successJsonResponse([],[
            'DepartmentType'=>$DepartmentType,
            'Insurance'=>$Insurance,
            'Services'=>$Services,
            'FacilityType'=>$FacilityType,
            'Specialization'=>$Specialization,
            'Nationality'=>$Nationality
        ]);
    }
    public function patients(Request $request){
        $logged = auth()->user();
        $Users = [];
        if($logged->type == User::TYPES['Facility']){
            $Orders = Order::where('facility_id',$logged->facility_id)->pluck('user_id');
            $Users = User::whereIn('id',$Orders)->paginate(($request->has('per_page'))?$request->per_page:15);
        }elseif ($logged->type == User::TYPES['Doctor']){
            $Orders = Order::where('doctor_id',$logged->id)->pluck('user_id');
            $Users = User::whereIn('id',$Orders)->paginate(($request->has('per_page'))?$request->per_page:15);
        }
        return $this->successJsonResponse([],$Users->items(),'Users',$Users);
    }
    public function message(Request $request,$id){
        $User = User::find($id);
        $logged = auth()->user();
        if($User->device_token)
            Master::sendNotification($User->id,$User->device_token,$request->title?($request->title):'لديك رسالة جديدة',$request->message,$request->ref_id,$request->type,false);
        return $this->successJsonResponse([]);
    }
}
