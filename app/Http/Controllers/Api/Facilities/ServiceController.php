<?php

namespace App\Http\Controllers\Api\Facilities;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Facilities\Services\UpdateForm;
use App\Models\Auth\User;
use App\Models\Facilities\FacilityInsurances;
use App\Models\Facilities\FacilityServices;
use App\Models\General\Insurance;
use App\Models\General\Service;
use App\Models\General\ServiceSection;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class ServiceController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * @return JsonResponse
     */
    public function index(){
        $Service = Service::all();
        return $this->successJsonResponse([], $Service,'Service');
    }

    /**
     * @param UpdateForm $form
     * @return JsonResponse
     */
    public function update(UpdateForm $form){
        return $form->persist();
    }
}

