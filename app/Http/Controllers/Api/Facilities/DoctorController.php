<?php

namespace App\Http\Controllers\Api\Facilities;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Facilities\Doctors\StoreForm;
use App\Http\Requests\Api\Facilities\Doctors\UpdateForm;
use App\Models\Auth\User;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DoctorController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request){
        $doctor =User::where('type',2)->where('facility_id',auth()->user()->facility_id)->orderBy('created_at', 'desc')->paginate($request->per_page?$request->per_page:10);
        return $this->successJsonResponse([],$doctor->items(),'Users',$doctor);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id){
        $user = User::where('id',$id)->first();
        if($user)
            return $this->successJsonResponse([],$user,'User');
        else
            return $this->failJsonResponse([__('messages.object_404')],[],'data',404);
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form){
        return $form->persist();
    }

    /**
     * @param UpdateForm $form
     * @param $id
     * @return JsonResponse
     */
    public function update(UpdateForm $form,$id){
        return $form->persist($id);
    }

    /**
     * @param UpdateForm $form
     * @param $id
     * @return JsonResponse
     */
    public function destroy(UpdateForm $form,$id){
        return $form->destroy($id);
    }
}

