<?php

namespace App\Http\Controllers\Api\Facilities;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Facilities\Insurances\UpdateForm;
use App\Models\Auth\User;
use App\Models\Facilities\FacilityInsurances;
use App\Models\General\Insurance;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;

class InsuranceController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * @return JsonResponse
     */
    public function index(){
        $Insurances = Insurance::all();
        return $this->successJsonResponse([], $Insurances,'Insurances');
    }

    /**
     * @param UpdateForm $form
     * @return JsonResponse
     */
    public function update(UpdateForm $form){
        return $form->persist();
    }
}

