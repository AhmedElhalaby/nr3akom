<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Facilities\Orders\PharmacyForm;
use App\Http\Requests\Api\Facilities\Orders\UpdateForm;
use App\Http\Requests\Api\Users\Orders\DoctorForm;
use App\Http\Requests\Api\Users\Orders\PharmacyUserForm;
use App\Http\Requests\Api\Users\Orders\SearchForm;
use App\Http\Requests\Api\Users\Orders\StoreForm;
use App\Models\Auth\User;
use App\Models\Facilities\Facility;
use App\Models\Facilities\FacilityServices;
use App\Models\Orders\Order;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    use ResponseTrait;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request){
        $Orders = new Order();
        if(auth()->user()->type == 1)
            $Orders = $Orders->where('facility_id',auth()->user()->facility_id);
        elseif (auth()->user()->type == 2)
            $Orders = $Orders->where('doctor_id',auth()->user()->id);
        elseif(auth()->user()->type == 3)
            $Orders = $Orders->where('user_id',auth()->user()->id);

        if($request->has('service_section_id'))
            $Orders = $Orders->where('service_section_id',$request->service_section_id);
        if($request->has('order_date'))
            $Orders = $Orders->where('order_date',$request->order_date);

        if($request->has('status')){
            if($request->status ==2)
                $Orders = $Orders->whereIn('status',[2,3,4]);
            else
                $Orders = $Orders->where('status',$request->status);
        }

        $Orders = $Orders->paginate($request->per_page?$request->per_page:10);
        return $this->successJsonResponse([], $Orders->items(),'Orders',$Orders);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function show($id){
        $Order = Order::where('id',$id);
        if(auth()->user()->type == 1)
            $Order = $Order->where('facility_id',auth()->user()->facility_id);
        elseif (auth()->user()->type == 2)
            $Order = $Order->where('doctor_id',auth()->user()->id);
        elseif(auth()->user()->type == 3)
            $Order = $Order->where('user_id',auth()->user()->id);
        return $this->successJsonResponse([], $Order->first(),'Order');
    }

    /**
     * @param SearchForm $request
     * @return JsonResponse
     */
    public function facilities(SearchForm $request){
        return $request->persist();
    }

    public function doctors(DoctorForm $request){
        return $request->persist();
    }

    /**
     * @param StoreForm $form
     * @return JsonResponse
     */
    public function store(StoreForm $form){
        return $form->persist();
    }

    /**
     * @param UpdateForm $form
     * @return JsonResponse
     */
    public function update(UpdateForm $form ,$id){
        return $form->persist($id);
    }

    /**
     * @param PharmacyForm $form
     * @return JsonResponse
     */
    public function pharmacy_response(PharmacyForm $form){
        return $form->persist();
    }

    /**
     * @param PharmacyUserForm $form
     * @return JsonResponse
     */
    public function pharmacy_user_response(PharmacyUserForm $form){
        return $form->persist();
    }

}

