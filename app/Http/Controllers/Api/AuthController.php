<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\Auth\FacilityRegistrationForm;
use App\Http\Requests\Api\Auth\LoginForm;
use App\Http\Requests\Api\Auth\PasswordRequest;
use App\Http\Requests\Api\Auth\RefreshForm;
use App\Http\Requests\Api\Auth\RegistrationForm;
use App\Http\Requests\Api\Auth\UserRegistrationForm;
use App\Http\Requests\Api\Auth\UserRequest;
use App\Traits\ResponseTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    use ResponseTrait;

    /**
     * Create user
     *
     * @param RegistrationForm $form
     * @return ResponseTrait
     */
    public function register(RegistrationForm $form)
    {
        return $form->persist();
    }

    /**
     * Login user and create token
     *
     * @param LoginForm $form
     * @return ResponseTrait
     */
    public function login(LoginForm $form)
    {
        return $form->persist();
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return ResponseTrait
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        $request->user()->update(['device_token'=>'']);
        return $this->successJsonResponse([__('auth.logout')]);
    }

    public function show(Request $request)
    {
        return $this->successJsonResponse([],$request->user(),'User');
    }

    /**
     * update user profile
     *
     * @param UserRequest $request
     * @return  ResponseTrait
     */
    public function update(UserRequest $request)
    {
        return $request->update();
    }

    /**
     * Refresh device token
     *
     * @param RefreshForm $request
     * @return  ResponseTrait
     */
    public function refresh(RefreshForm $request){
         return $request->refresh();
    }
    /**
     * @param PasswordRequest $request
     * @return JsonResponse
     */
    public function change_password(PasswordRequest $request){
        return $request->update();
    }

    /**
     * Refresh device token
     *
     * @param FacilityRegistrationForm $request
     * @return  ResponseTrait
     */
    public function facility_registration(FacilityRegistrationForm $request){
         return $request->complete();
    }
    /**
     * Refresh device token
     *
     * @param UserRegistrationForm $request
     * @return  ResponseTrait
     */
    public function user_registration(UserRegistrationForm $request){
         return $request->complete();
    }
}
