<?php

namespace App\Http\Controllers\Facility;

use App\Http\Requests\Facility\Orders\UpdateForm;
use App\Master;
use App\Models\Auth\Doctor;
use App\Models\Auth\User;
use App\Models\General\Notification;
use App\Models\General\SpecializationService;
use App\Models\Orders\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    public function index(Request $request){
        $Objects = new Order();
        $user_name = '0';
        $Objects = $Objects->where('facility_id',auth()->user()->facility_id);
        if($request->has('patient')){
            $Objects = $Objects->where('user_id',$request->patient);
            $User = User::find($request->patient);
            if($User){
                $user_name = $User->name;
            }else{
                $user_name = '$User->name';
            }
        }
        if($request->has('doctor')){
            $Objects = $Objects->where('doctor_id',$request->doctor);
            $User = User::find($request->doctor);
            if($User){
                $user_name = $User->name;
            }else{
                $user_name = '$User->name';
            }
        }
        $Objects = $Objects->paginate(10);
        return view('facility.orders.index',compact('Objects','user_name'));
    }
    public function show($id){
        $Object = Order::find($id);
        $Spes = SpecializationService::where('service_section_id',$Object->service_section_id)->pluck('specialization_id');
        $Users = User::where('facility_id',$Object->facility_id)->where('type',2)->pluck('id');
        $Doctor = Doctor::whereIn('user_id',$Users)->whereIn('specialization_id',$Spes)->pluck('user_id');
        $Doctors = User::whereIn('id',$Doctor)->get();
        return view('facility.orders.show',compact('Object','Doctors'));
    }

    public function notification(Request $request ,$id){
        $Object = Order::find($id);
        if($Object){
            $Title = $request->has('title')?$request->title:'';
            $Message = $request->has('message')?$request->message:'';
            $User = $Object->user;
            if($User->device_token != null)
                Master::sendNotification($User->id,$User->device_token,$Title,$Message,$Object->facility_id,Notification::TYPE['Facility']);
            return redirect()->back()->with('status', __('facility.messages.notification_sent'));
        }
        return redirect()->back();
    }
    public function update($id,UpdateForm $request){
        return $request->persist($id);
    }
}
