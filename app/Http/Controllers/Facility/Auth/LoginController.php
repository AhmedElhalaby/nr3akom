<?php

namespace App\Http\Controllers\Facility\Auth;

use App\Http\Controllers\Facility\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/facility';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        App::setLocale(session('my_locale','en'));
        $this->middleware('guest')->except('logout');
    }
    public function username()
    {
        return 'email';
    }
    protected function guard()
    {
        return Auth::guard('web');
    }
    public function showLoginForm()
    {
        return view('facility.auth.login');
    }
}
