<?php

namespace App\Http\Controllers\Facility\Auth;

use App\Http\Requests\Facility\Auth\UserRequest;
use App\Models\Auth\User;
use App\Http\Controllers\Facility\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/facility';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        App::setLocale(session('my_locale','en'));
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return Response
     */
    public function showRegistrationForm()
    {
        return view('facility.auth.register');
    }
    /**
     * Show the application complete registration form.
     *
     * @return Response
     */
    public function showCompleteRegistrationForm()
    {
        return view('facility.auth.complete_register');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'gender' => [Rule::in([1,2])],
            'name' => 'required|string|max:255',
        ]);
    }
    protected function guard()
    {
        return Auth::guard('web');
    }
    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'type' => 1,
            'mobile' => $data['mobile'],
            'gender' => (@$data['gender'])?$data['gender']:1,
        ]);
    }

    public function CompleteRegister(UserRequest $request)
    {
        return $request->preset();
    }

}
