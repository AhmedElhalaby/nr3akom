<?php

namespace App\Http\Controllers\Facility\Auth;

use App\Http\Requests\Facility\Auth\UserRequest;
use App\Http\Controllers\Facility\Controller;
use Illuminate\Http\Response;

class CompleteRegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Show the application complete registration form.
     *
     * @return Response
     */
    public function showCompleteRegistrationForm()
    {
        return view('facility.auth.complete_register');
    }

    public function CompleteRegister(UserRequest $request)
    {
        return $request->preset();
    }

}
