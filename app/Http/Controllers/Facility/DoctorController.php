<?php

namespace App\Http\Controllers\Facility;

use App\Http\Requests\Facility\Doctors\UpdatePasswordRequest;
use App\Http\Requests\Facility\Doctors\StoreForm;
use App\Http\Requests\Facility\Doctors\UpdateForm;
use App\Models\Auth\User;

class DoctorController extends Controller
{
    public function index(){
        $Objects = User::where('facility_id',auth()->user()->facility_id)->where('type',User::TYPES['Doctor'])->paginate(10);
        return view('facility.doctors.index',compact('Objects'));
    }
    public function create(){
        return view('facility.doctors.create');
    }
    public function store(StoreForm $request){
        return $request->persist();
    }

    public function edit($id){
        $Object = User::find($id);
        return view('facility.doctors.edit',compact('Object'));
    }
    public function update($id,UpdateForm $request){
        return $request->persist($id);
    }
    /**
     * Update the Password resource in storage.
     *
     * @param UpdatePasswordRequest $request
     * @return UpdatePasswordRequest
     */
    public function updatePassword(UpdatePasswordRequest $request)
    {
        return $request->preset();
    }
    public function destroy(UpdateForm $request){
        return $request->destroy();
    }
}
