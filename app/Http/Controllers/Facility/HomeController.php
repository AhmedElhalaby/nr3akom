<?php

namespace App\Http\Controllers\Facility;

use App\Http\Requests\Facility\Auth\UserRequest;
use App\Master;
use App\Models\Auth\User;
use App\Models\General\Notification;
use App\Models\Orders\Order;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index(){
        $NewOrders = Order::where('facility_id',auth()->user()->facility_id)->where('status',Order::Status['New'])->take(10)->get();
        $TodayOrders = Order::where('facility_id',auth()->user()->facility_id)->where('status',Order::Status['Accepted'])->where('order_date',now()->format('Y-m-d'))->take(10)->get();
        return view('facility.index',compact('NewOrders','TodayOrders'));
    }
    public function patients(){
        $patients = Order::where('facility_id',auth()->user()->facility_id)->pluck('user_id');
        $Objects = User::whereIn('id',$patients)->paginate(15);
        return view('facility.patients',compact('Objects'));
    }
    public function notification(Request $request ){
        $Title = $request->has('title')?$request->title:'';
        $Message = $request->has('message')?$request->message:'';
        if($request->has('type') && ( $request->type == 1 ||  $request->type == 0)){
            $Doctors = User::where('facility_id',auth()->user()->facility_id)->where('type',User::TYPES['Doctor'])->get();
            foreach ($Doctors as $Doctor){
                if($Doctor->device_token != null)
                    Master::sendNotification($Doctor->id,$Doctor->device_token,$Title,$Message,auth()->user()->facility_id,Notification::TYPE['Facility']);
            }
        }
        if($request->has('type') && ( $request->type == 2 ||  $request->type == 0)){
            $patients = Order::where('facility_id',auth()->user()->facility_id)->pluck('user_id');
            $Patients = User::whereIn('id',$patients)->paginate(15);
            foreach ($Patients as $Patient){
                if($Patient->device_token != null)
                    Master::sendNotification($Patient->id,$Patient->device_token,$Title,$Message,auth()->user()->facility_id,Notification::TYPE['Facility']);
            }
        }
        return redirect()->back()->with('status', __('admin.messages.notification_sent'));
    }
    public function profile(){
        return view('facility.profile');
    }
    public function update_profile(UserRequest $form){
        return $form->preset();
    }
}
