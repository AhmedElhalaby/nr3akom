<?php

namespace App\Http\Requests\Facility\Auth;

use App\Models\Facilities\Facility;
use App\Models\Facilities\FacilityDepartments;
use App\Models\Facilities\FacilityInsurances;
use App\Models\Facilities\FacilityServices;
use App\Models\General\DepartmentType;
use App\Models\General\Insurance;
use App\Models\General\ServiceSection;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'facility_type_id' => 'required|exists:facility_types,id',
            'license_num' => 'required|string',
            'commercial_register_num' => 'required|string',
//            'license_copy' => 'required|mimes:jpeg,jpg,bmp,png',
//            'commercial_register_copy' => 'required|mimes:jpeg,bmp,png',
        ];
    }

    public function preset()
    {
        $logged = auth()->user();
        if($logged->facility)
            $facility = $logged->facility;
        else
            $facility = new Facility();
        $facility->name = $this->name;
        $facility->facility_type_id = $this->facility_type_id;
        $facility->license_num =@$this->license_num;
        if($this->hasFile('license_copy'))
            $facility->license_copy = $this->license_copy;
        $facility->commercial_register_num = @$this->commercial_register_num;
        if($this->hasFile('commercial_register_copy'))
            $facility->commercial_register_copy = $this->commercial_register_copy;
        if ($facility->save()) {
            FacilityInsurances::where('facility_id',$facility->id)->delete();
            FacilityServices::where('facility_id',$facility->id)->delete();
            foreach ($this->supported_insurances as $supported_insurance) {
                $insurance = Insurance::where('id', $supported_insurance)->first();
                $FacilityInsurance = FacilityInsurances::where('facility_id',$facility->id)->where('insurance_id',$supported_insurance)->first();
                if ($insurance != null) {
                    $FacilityInsurance = new FacilityInsurances();
                    $FacilityInsurance->insurance_id = $insurance->id;
                    $FacilityInsurance->facility_id = $facility->id;
                    $FacilityInsurance->save();
                }
            }
            foreach ($this->services as $service) {
                $service = ServiceSection::where('id', $service)->first();
                if ($service != null) {
                    $FacilityService = new FacilityServices();
                    $FacilityService->service_section_id = $service->id;
                    $FacilityService->facility_id = $facility->id;
                    $FacilityService->save();
                }
            }
        }
        $logged->facility_id = $facility->id;
        $logged->save();
        return redirect('facility');
    }
}
