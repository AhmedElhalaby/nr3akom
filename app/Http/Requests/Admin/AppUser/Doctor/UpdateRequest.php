<?php

namespace App\Http\Requests\Admin\AppUser\Doctor;

use App\Models\Auth\NormalUser;
use App\Models\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'mobile'=>'required',
            'gender' => 'required|in:1,2',
            'nationality_id'=>'required|exists:nationalities,id',
            'specialization_id'=>'required|exists:specializations,id',
            'address'=>'required',
            'national_address'=>'required',
            'identity'=>'required',
            'dob'=>'required',
            'identity_image' => 'mimes:jpeg,jpg,bmp,png',
            'image' => 'mimes:jpeg,jpg,bmp,png',
        ];
    }
    public function preset($redirect,$id){
        $Object = User::find($id);
        $Object->name = $this->name;
        $Object->email = $this->email;
        $Object->mobile = $this->mobile;
        $Object->gender = $this->gender;
        if($this->has('image'))
            $Object->image = $this->image;
        $Object->save();
        $Doctor= $Object->doctor;
        $Doctor->nationality_id = $this->nationality_id;
        $Doctor->specialization_id = $this->specialization_id;
        $Doctor->address = $this->address;
        $Doctor->national_address = $this->national_address;
        $Doctor->identity = $this->identity;
        $Doctor->dob = $this->dob;
        if($this->hasFile('identity_image'))
            $Doctor->identity_image = $this->identity_image;
        $Doctor->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
