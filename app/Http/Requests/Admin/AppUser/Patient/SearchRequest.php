<?php

namespace App\Http\Requests\Admin\AppUser\Patient;

use App\Models\Auth\NormalUser;
use App\Models\Auth\User;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new User();
        $Objects = $Objects->where('type',User::TYPES['NormalUser']);
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%');
        }
        if($this->has('name') && $this->name != ''){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%');
        }
        if($this->has('email') && $this->email != ''){
            $Objects = $Objects->where('email','LIKE','%'.$this->email.'%');
        }
        if($this->has('mobile') && $this->mobile != ''){
            $Objects = $Objects->where('mobile','LIKE','%'.$this->mobile.'%');
        }
        if($this->has('gender') && $this->gender != ''){
            $Objects = $Objects->where('gender','LIKE','%'.$this->gender.'%');
        }
        if($this->has('is_active') && $this->is_active != ''){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->is_active.'%');
        }
        if($this->has('nationality_id') && $this->nationality_id != ''){
            $NormalUser = NormalUser::where('nationality_id',$this->nationality_id)->pluck('user_id');
            $Objects = $Objects->whereIn('id',$NormalUser);
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
