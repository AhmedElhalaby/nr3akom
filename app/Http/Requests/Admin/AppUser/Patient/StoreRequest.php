<?php

namespace App\Http\Requests\Admin\AppUser\Patient;

use App\Models\Auth\NormalUser;
use App\Models\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'mobile'=>'required',
            'gender' => 'required|in:1,2',
            'nationality_id'=>'required|exists:nationalities,id',
            'insurance_id'=>'required|exists:insurances,id',
            'address'=>'required',
            'identity'=>'required',
            'dob'=>'required',
            'medical_status'=>'required',
            'identity_image' => 'required|mimes:jpeg,jpg,bmp,png',
            'insurance_image' => 'required|mimes:jpeg,jpg,bmp,png',
            'image' => 'mimes:jpeg,jpg,bmp,png',
        ];
    }
    public function preset($redirect){
        $Object = new User();
        $Object->name = $this->name;
        $Object->email = $this->email;
        $Object->password = Hash::make($this->password);
        $Object->mobile = $this->mobile;
        $Object->gender = $this->gender;
        if($this->has('image'))
            $Object->image = $this->image;
        $Object->type = User::TYPES['NormalUser'];
        $Object->save();
        $NormalUser= new NormalUser();
        $NormalUser->user_id = $Object->id;
        $NormalUser->nationality_id = $this->nationality_id;
        $NormalUser->insurance_id = $this->insurance_id;
        $NormalUser->address = $this->address;
        $NormalUser->identity = $this->identity;
        $NormalUser->dob = $this->dob;
        $NormalUser->medical_status = $this->medical_status;
        $NormalUser->identity_image = $this->identity_image;
        $NormalUser->insurance_image = $this->insurance_image;
        $NormalUser->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
