<?php

namespace App\Http\Requests\Admin\AppSetting\Specialization;

use App\Models\General\Specialization;
use App\Models\General\SpecializationService;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'services' => 'exists:service_sections,id',
        ];
    }
    public function preset($redirect){
        $Object = Specialization::create(array('name' => $this->name,'ar_name' => $this->ar_name, 'description' => ($this->has('description'))?$this->description:'','ar_description' => ($this->has('ar_description'))?$this->ar_description:''));
        $services = $this->has('services')?$this->services:[];
        foreach ($services as $service){
            SpecializationService::create(['specialization_id'=>$Object->id,'service_section_id'=>$service]);
        }
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
