<?php

namespace App\Http\Requests\Admin\AppSetting\Specialization;

use App\Models\General\Specialization;
use Illuminate\Foundation\Http\FormRequest;

class DestroyRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|exists:facility_types,id',
        ];
    }
    public function preset($redirect){
        $Object = Specialization::find($this->id);
        $Object->delete();
        return redirect($redirect)->with('status', __('admin.messages.deleted_successfully'));
    }
}
