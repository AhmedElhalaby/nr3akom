<?php

namespace App\Http\Requests\Admin\AppSetting\Specialization;

use App\Models\General\Specialization;
use App\Models\General\SpecializationService;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'services' => 'exists:service_sections,id',
        ];
    }
    public function preset($redirect,$id){
        $services = $this->has('services')?$this->services:[];
        $Object = Specialization::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->update(array('ar_name' => $this->ar_name,'name' => $this->name, 'description' => ($this->has('description'))?$this->description:$Object->description,'ar_description' => ($this->has('ar_description'))?$this->ar_description:$Object->ar_description));
        SpecializationService::where('specialization_id',$id)->delete();
        foreach ($services as $service){
            SpecializationService::create(['specialization_id'=>$id,'service_section_id'=>$service]);
        }
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
