<?php

namespace App\Http\Requests\Admin\AppSetting\ServiceSection;

use App\Models\General\ServiceSection;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new ServiceSection();
        $Objects = $Objects->where('service_id',$this->route('id'));
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orWhere('ar_name','LIKE','%'.$this->q.'%');
        }

        if($this->has('active')){
            $Objects = $Objects->where('is_active','LIKE','%'.$this->active.'%');
        }

        if($this->has('name') && $this->name != ''){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%')->orWhere('ar_name','LIKE','%'.$this->name.'%');
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
