<?php

namespace App\Http\Requests\Admin\AppSetting\ServiceSection;

use App\Models\General\ServiceSection;
use Illuminate\Foundation\Http\FormRequest;

class ActivationRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($redirect){
        $Object = ServiceSection::find($this->route('id2'));
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->update(array('is_active' => ($Object->is_active)?false:true));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
