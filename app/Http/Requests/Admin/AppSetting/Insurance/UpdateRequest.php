<?php

namespace App\Http\Requests\Admin\AppSetting\Insurance;

use App\Models\General\Insurance;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
            'website' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'phone' => 'required|string|max:255',
        ];
    }
    public function preset($redirect,$id){
        $Object = Insurance::find($id);
        if(!$Object)
            return redirect($redirect)->withErrors(__('admin.messages.wrong_data'));
        $Object->name = $this->name;
        $Object->ar_name = $this->ar_name;
        $Object->website = $this->website;
        $Object->address = $this->address;
        $Object->phone = $this->phone;
        $Object->phone2 = ($this->has('phone2'))?$this->phone2:$Object->phone2;
        $Object->save();
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
