<?php

namespace App\Http\Requests\Admin\AppSetting\Insurance;

use App\Models\General\Insurance;
use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        ];
    }
    public function preset($view,$params){
        $Objects = new Insurance();
        if($this->has('q')){
            $Objects = $Objects->where('name','LIKE','%'.$this->q.'%')->orWhere('ar_name','LIKE','%'.$this->q.'%')->orWhere('website','LIKE','%'.$this->q.'%')->orWhere('address','LIKE','%'.$this->q.'%')->orWhere('phone','LIKE','%'.$this->q.'%')->orWhere('phone2','LIKE','%'.$this->q.'%');
        }

        if($this->has('website') && $this->website != ''){
            $Objects = $Objects->where('website','LIKE','%'.$this->website.'%');
        }
        if($this->has('address') && $this->address != ''){
            $Objects = $Objects->where('address','LIKE','%'.$this->address.'%');
        }
        if($this->has('phone') && $this->phone != ''){
            $Objects = $Objects->where('phone','LIKE','%'.$this->phone.'%');
        }
        if($this->has('phone2') && $this->phone2 != ''){
            $Objects = $Objects->where('phone2','LIKE','%'.$this->phone2.'%');
        }

        if($this->has('name') && $this->name != ''){
            $Objects = $Objects->where('name','LIKE','%'.$this->name.'%')->orWhere('ar_name','LIKE','%'.$this->name.'%');
        }
        $Objects = $Objects->paginate(($this->per_page)?$this->per_page:10);
        return view($view,compact('Objects'))->with($params);
    }
}
