<?php

namespace App\Http\Requests\Admin\AppSetting\Nationality;

use App\Models\General\Nationality;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'ar_name' => 'required|string|max:255',
        ];
    }
    public function preset($redirect){
        $Object = Nationality::create(array('name' => $this->name,'ar_name' => $this->ar_name));
        return redirect($redirect)->with('status', __('admin.messages.saved_successfully'));
    }
}
