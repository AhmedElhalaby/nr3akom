<?php

namespace App\Http\Requests\Api\Users\Orders;

use App\Master;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Auth\Doctor;
use App\Models\General\Notification;
use App\Models\General\Service;
use App\Models\General\ServiceSection;
use App\Models\Orders\Order;
use App\Traits\ResponseTrait;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class StoreForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'facility_id' => 'required|exists:facilities,id',
            'service_section_id' => 'required|exists:service_sections,id',
            'doctor_id' => 'exists:users,id',
            'order_date' => 'string|max:255',
            'order_time' => 'string|max:255',
            'description' => 'string|max:255',
            'lat' => 'string|max:255',
            'lng' => 'string|max:255',
            'attachment.*'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Orders');
    }
    public function persist()
    {
        $logged = auth()->user();
        $ServiceSection = ServiceSection::find($this->service_section_id);
        if($ServiceSection->service_id == 6 && !$this->hasFile('attachment'))
            return $this->failJsonResponse([__('messages.image_required')]);
        $Order = new Order();
        $Order->user_id = $logged->id;
        $Order->facility_id = $this->facility_id;
        $Order->service_section_id = $this->service_section_id;
        $Order->doctor_id = ($this->has('doctor_id'))?$this->doctor_id:null;
        $Order->order_date = ($this->has('order_date'))? date("Y-m-d",strtotime($this->order_date)):null;
        $Order->description = ($this->has('description'))?$this->description:null;
        $Order->lat = ($this->has('lat'))?$this->lat:null;
        $Order->lng = ($this->has('lng'))?$this->lng:null;
        $Order->save();
        if($ServiceSection->service_id == 6)
            Master::MultiUpload('attachment','orders',$Order->id,6);
        $title = 'لديك طلب جديد';
        $message = 'لديك طلب جديد من قبل '.$Order->user->name.'';
        $User = User::where('facility_id',$this->facility_id)->where('type',User::TYPES['Facility'])->first();
        Master::sendNotification($User->id,$User->device_token,$title,$message,$Order->id,Notification::TYPE['Order'],true);
        if($this->has('doctor_id')){
            Master::sendNotification($Order->doctor->id,$Order->doctor->device_token,$title,$message,$Order->id,Notification::TYPE['Order'],true);
        }
        return $this->successJsonResponse( [__('messages.create_successful')],$Order,'Order');
    }

}
