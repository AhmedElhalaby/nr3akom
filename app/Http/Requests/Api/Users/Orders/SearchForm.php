<?php

namespace App\Http\Requests\Api\Users\Orders;

use App\Master;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Facilities\Facility;
use App\Models\Facilities\FacilityServices;
use App\Traits\ResponseTrait;

class SearchForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $logged = auth()->user();
        if(!$this->has('service_section_id'))
            return $this->successJsonResponse( [],[],'Facilities');

        $FacilityServices = FacilityServices::where('service_section_id',$this->service_section_id)->pluck('facility_id');
        if(!$FacilityServices)
            $FacilityServices = [];
        $Facilities = Facility::whereIn('id',$FacilityServices)->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Facilities->items(),'Facilities',$Facilities);
    }

}
