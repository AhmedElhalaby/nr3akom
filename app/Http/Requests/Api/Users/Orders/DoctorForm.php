<?php

namespace App\Http\Requests\Api\Users\Orders;

use App\Master;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Auth\Doctor;
use App\Models\Facilities\Facility;
use App\Models\Facilities\FacilityServices;
use App\Models\General\Service;
use App\Models\General\ServiceSection;
use App\Models\General\SpecializationService;
use App\Models\Orders\Order;
use App\Traits\ResponseTrait;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class DoctorForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'facility_id' => 'required|exists:facilities,id',
            'service_section_id' => 'required|exists:service_sections,id',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $logged = auth()->user();
        $Spes = SpecializationService::where('service_section_id',$this->service_section_id)->pluck('specialization_id');
        $Users = User::where('facility_id',$this->facility_id)->where('type',2)->pluck('id');
        $Doctor = Doctor::whereIn('user_id',$Users)->whereIn('specialization_id',$Spes)->pluck('user_id');

        $Doctors = User::whereIn('id',$Doctor)->paginate($this->has('per_page')?$this->per_page:10);
        return $this->successJsonResponse( [],$Doctors->items(),'Users',$Doctors);
    }

}
