<?php

namespace App\Http\Requests\Api\Users\Orders;

use App\Master;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Orders\Order;
use App\Traits\ResponseTrait;

class PharmacyUserForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'status'=>'required|in:0,1',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $logged = auth()->user();
        $order = Order::find($this->order_id);
        if(!$order && $order->status == 2){
            if($this->status == 1)
                $order->order_state = 3;
            else{
                $order->order_state = 4;
                $order->status = 4;
            }
            $order->save();
            return $this->successJsonResponse([__('messages.response_successful')],$order,'Order');
        }

        else
            return $this->failJsonResponse([__('messages.object_404')],[],'data',[],404);
    }
}
