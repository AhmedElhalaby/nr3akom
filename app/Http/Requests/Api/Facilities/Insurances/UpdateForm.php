<?php

namespace App\Http\Requests\Api\Facilities\Insurances;

use App\Master;
use App\Models\Facilities\FacilityInsurances;
use Illuminate\Foundation\Http\FormRequest;

class UpdateForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'insurance_id' => 'required',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth()->user();
        $FacilityInsurances = FacilityInsurances::where('facility_id',$logged->facility_id)->where('insurance_id',$this->insurance_id)->first();
        if($FacilityInsurances){
            $FacilityInsurances->delete();
            return $this->successJsonResponse([__('messages.update_successful')],[],'data',[],204);
        }
        else{
            $FacilityInsurance= new FacilityInsurances();
            $FacilityInsurance->insurance_id = $this->insurance_id;
            $FacilityInsurance->facility_id = $logged->facility_id;
            $FacilityInsurance->save();
            return $this->successJsonResponse([__('messages.update_successful')],[],'data',[],201);
        }
    }
}
