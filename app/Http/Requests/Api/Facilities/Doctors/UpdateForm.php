<?php

namespace App\Http\Requests\Api\Facilities\Doctors;

use App\Master;
use App\Models\Auth\Doctor;
use App\Models\Auth\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class UpdateForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist($id)
    {
        $logged = auth()->user();
        $user = User::find($id);
        if ($user) {
            $doctor = Doctor::where('user_id', $user->id)->first();
            $user->name = $this->name?$this->name:$user->name;
            $user->email = $this->email?$this->email:$user->email;
            $user->password =$this->password? Hash::make($this->password):$user->password;
            $user->mobile = $this->mobile?$this->mobile:$user->mobile;
            $user->image = $this->image?$this->image:$user->image;
            $user->gender = ($this->gender)?$this->gender:$user->gender;
            $user->save();
            $doctor->nationality=($this->nationality)?$this->nationality : $doctor->nationality;
            $doctor->address=($this->address)?$this->address : $doctor->address;
            $doctor->national_address=($this->national_address)?$this->national_address : $doctor->national_address;
            $doctor->identity_num=($this->identity_num)?$this->identity_num : $doctor->identity_num;
            $doctor->identity_image=($this->identity_image)?$this->identity_image : $doctor->identity_image;
            $doctor->specialization_id=($this->specialization_id)?$this->specialization_id : $doctor->specialization_id;
            $doctor->c_schs=($this->c_schs)?$this->c_schs : $doctor->c_schs;
            $doctor->prc_schs=($this->prc_schs)?$this->prc_schs : $doctor->prc_schs;
            $doctor->license=($this->license)?$this->license : $doctor->license;
            $doctor->phone=($this->phone)?$this->phone : $doctor->phone;
            $doctor->phone2=($this->phone2)?$this->phone2 : $doctor->phone2;
            $doctor->birthday=($this->birthday)?$this->birthday : $doctor->birthday;
            $doctor->details=($this->details)?$this->details : $doctor->details;
            $doctor->save();
            return $this->successJsonResponse([__('messages.update_successful')],$user,'User');
        }
        else
            return $this->failJsonResponse([__('messages.object_404')],[],'data',404);
    }
    public function destroy($id){
        $logged = auth()->user();
        $user = User::find($id);
        if ($user != null){
            if($logged->facility_id == $user->facility_id && $logged->type == 1){
                $user->delete();
                return $this->successJsonResponse([__('messages.delete_successful')]);
            }else{
                return $this->failJsonResponse([__('messages.object_403')],[],'data',403);
            }
        }else{
            return $this->failJsonResponse([__('messages.object_404')],[],'data',404);
        }
    }
}
