<?php

namespace App\Http\Requests\Api\Facilities\Doctors;

use App\Master;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Auth\Doctor;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class StoreForm extends ApiRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'name' => 'required|string|max:255',
            'mobile' => 'required|string|max:255',
            'gender' => [Rule::in([1,2])],
            'image' => 'required|mimes:jpeg,bmp,png',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Doctor');
    }
    public function persist()
    {
        $logged = auth()->user();
        $user = new User([
            'name' => $this->name,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'mobile' => $this->mobile,
            'image' => $this->image,
            'type' => User::TYPES['Doctor'],
            'gender' => ($this->gender)?$this->gender:1,
            'facility_id' => $logged->facility_id,
        ]);
        if($user->save()){
            $doctor = new Doctor([
                'user_id'=>$user->id,
                'nationality'=>($this->nationality)?$this->nationality : null,
                'address'=>($this->address)?$this->address : null,
                'national_address'=>($this->national_address)?$this->national_address : null,
                'identity_num'=>($this->identity_num)?$this->identity_num : null,
                'identity_image'=>($this->identity_image)?$this->identity_image : null,
                'specialization_id'=>($this->specialization_id)?$this->specialization_id : null,
                'c_schs'=>($this->c_schs)?$this->c_schs : null,
                'prc_schs'=>($this->prc_schs)?$this->prc_schs : null,
                'license'=>($this->license)?$this->license : null,
                'phone'=>($this->phone)?$this->phone : null,
                'phone2'=>($this->phone2)?$this->phone2 : null,
                'birthday'=>($this->birthday)?$this->birthday : null,
                'details'=>($this->details)?$this->details : null,
            ]);
            $doctor->save();
        }
        return $this->successJsonResponse( [__('messages.create_successful')],$user,'User');
    }

}
