<?php

namespace App\Http\Requests\Api\Facilities\Services;

use App\Master;
use App\Models\Facilities\FacilityServices;
use App\Traits\ResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class UpdateForm extends FormRequest
{
    use ResponseTrait;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'service_section_id' => 'required',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $logged = auth()->user();
        $FacilityServices = FacilityServices::where('facility_id',$logged->facility_id)->where('service_section_id',$this->service_section_id)->first();
        if($FacilityServices){
            $FacilityServices->delete();
            return $this->successJsonResponse([__('messages.update_successful')],[],'data',[],204);
        }
        else{
            $FacilityService= new FacilityServices();
            $FacilityService->service_section_id = $this->service_section_id;
            $FacilityService->facility_id = $logged->facility_id;
            $FacilityService->save();
            return $this->successJsonResponse([__('messages.update_successful')],[],'data',[],201);
        }
    }
}
