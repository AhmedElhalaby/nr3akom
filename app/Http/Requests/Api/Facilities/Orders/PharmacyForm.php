<?php

namespace App\Http\Requests\Api\Facilities\Orders;

use App\Master;
use App\Models\Orders\Order;
use Illuminate\Foundation\Http\FormRequest;

class PharmacyForm extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'order_id'=>'required|exists:orders,id',
            'delivery_method'=>'required',
            'delivery_cost'=>'required_if:delivery_method,2',
            'price'=>'required',
            'instructions'=>'required',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Order');
    }
    public function persist()
    {
        $logged = auth()->user();
        $order = Order::find($this->order_id);
        if(!$order && $order->status != 1 && $order->order_state != 1)
            return $this->failJsonResponse([__('messages.object_404')],[],'data',[],404);
        if($logged->type != 1 && $order->facility_id != $logged->facility_id)
            return $this->failJsonResponse([__('messages.object_404')],[],'data',[],404);
        if($logged->type != 2 && $order->doctor_id != $logged->id)
            return $this->failJsonResponse([__('messages.object_404')],[],'data',[],404);

        $order->delivery_method = $this->delivery_method;
        $order->delivery_cost = ($this->delivery_cost)?$this->delivery_cost:0;
        $order->delivery_duration = ($this->delivery_duration)?$this->delivery_duration:0;
        $order->price = $this->price;
        $order->instructions = $this->instructions;
        $order->description = ($this->description)?$this->description:$order->description;
        $order->order_state = 2;
        $order->save();
        return $this->successJsonResponse([__('messages.response_successful')],$order,'Order');
    }
}
