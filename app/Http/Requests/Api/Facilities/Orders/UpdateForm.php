<?php

namespace App\Http\Requests\Api\Facilities\Orders;

use App\Master;
use App\Models\Auth\User;
use App\Models\General\Notification;
use App\Models\Orders\Order;
use App\Traits\ResponseTrait;
use Illuminate\Foundation\Http\FormRequest;

class UpdateForm extends FormRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => 'required_without:order_state',
            'order_state' => 'required_without:status',
            'doctor_id' => 'exists:users,id',
            'delivery_method'=>'required_if:order_state,2',
            'delivery_cost'=>'required_if:delivery_method,2',
            'price'=>'required_if:order_state,2',
            'instructions'=>'required_if:order_state,2',
        ];
    }

    public function attributes()
    {
        return Master::NiceNames('Order');
    }

    public function persist($id)
    {
        $logged = auth()->user();
        $order = Order::find($id);

        if ($order) {
            if($this->has('status')){
                switch ($this->status) {
                    case Order::Status['Accepted']:
                    {
                        if($order->facility_id != $logged->facility_id || $logged->type != User::TYPES['Facility'])
                            return $this->failJsonResponse([__('messages.dont_have_permission')], [], 'data',[], 403);
                        $order->status = $this->status;
                        $title = 'لقد تم قبول طلبك';
                        $message = 'لقد تم قبول طلبك';
                        Master::sendNotification($order->user_id,$order->user->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        if ($this->has('order_time_from')) {
                            $order->order_time_from = $this->order_time_from;
                        }
                        if ($this->has('order_time_to')) {
                            $order->order_time_to = $this->order_time_to;
                        }
                        if ($this->has('order_date')) {
                            $order->order_date = $this->order_date;
                        }
                        if ($this->has('doctor_id')) {
                            $Doctor = User::where('id', $this->doctor_id)->where('type', User::TYPES['Doctor'])->first();
                            if ($Doctor)
                                $order->doctor_id = $this->doctor_id;
                            $title = 'يوجد لديك طلب جديد';
                            $message = 'يوجد لديك طلب جديد';
                            Master::sendNotification($Doctor->id,$Doctor->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        }
                        break;
                    }
                    case Order::Status['Finished']:
                    {
                        if($order->facility_id != $logged->facility_id || $logged->type == User::TYPES['NormalUser'])
                            return $this->failJsonResponse([__('messages.dont_have_permission')], [], 'data',[], 403);
                        $order->status = $this->status;
                        $title = 'لقد تم إنهاء طلبك';
                        $message = 'لقد تم إنهاء طلبك';
                        Master::sendNotification($order->user_id,$order->user->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        break;
                    }
                    case Order::Status['Cancel']:{
                        if($order->status != Order::Status['Rejected']||$order->status != Order::Status['Finished']){
                            $order->description = ($this->description) ? $this->description : '';
                            $order->status = $this->status;
                            $title = 'لقد تم إلغاء الطلب';
                            $message = 'لقد تم إلغاء الطلب من قبل '.$order->user->name.'';
                            Master::sendNotification($order->facility->user->id,$order->facility->user->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        }else{
                            return $this->failJsonResponse([__('messages.dont_have_permission')], [], 'data',[], 403);
                        }
                        break;
                    }
                    case Order::Status['Rejected']:
                    {
                        $order->description = ($this->description) ? $this->description : '';
                        $order->status = $this->status;
                        $title = 'لقد تم رفض طلبك';
                        $message = 'لقد تم رفض طلبك';
                        Master::sendNotification($order->user_id,$order->user->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        break;
                    }
                    default:
                        break;
                }
            }elseif ($this->has('order_state')){
                switch ($this->order_state) {
                    case Order::OrderState['PharmacyReply']:
                    {
                        if(!$order || $order->status != 1 || $order->order_state != 1)
                            return $this->failJsonResponse([__('messages.object_401')],[],'data',[],404);
                        if($logged->type == 1 && $order->facility_id != $logged->facility_id)
                            return $this->failJsonResponse([__('messages.object_402')],[],'data',[],404);
                        if($logged->type == 2 && $order->doctor_id != $logged->id)
                            return $this->failJsonResponse([__('messages.object_403')],[],'data',[],404);
                        if($order->type == 3)
                            return $this->failJsonResponse([__('messages.object_404')],[],'data',[],404);

                        $order->delivery_method = $this->delivery_method;
                        $order->delivery_cost = ($this->delivery_cost)?$this->delivery_cost:0;
                        $order->delivery_duration = ($this->delivery_duration)?$this->delivery_duration:0;
                        $order->price = $this->price;
                        $order->instructions = $this->instructions;
                        $order->description = ($this->description)?$this->description:$order->description;
                        $order->order_state = $this->order_state;
                        $title = 'لقد تم الرد على طلبك';
                        $message = 'لقد تم الرد على طلبك';
                        Master::sendNotification($order->user_id,$order->user->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        break;
                    }
                    case Order::OrderState['UserConfirm']:
                    {
                        if($order->user_id != $logged->id || $logged->type != 3)
                            return $this->failJsonResponse([__('messages.dont_have_permission')], [], 'data',[], 403);
                        $order->order_state = $this->order_state;
                        $title = 'لقد تم تأكيد الطلب';
                        $message = 'لقد تم تأكيد الطلب من قبل '.$order->user->name.'';
                        Master::sendNotification($order->facility->user->id,$order->facility->user->device_token,$title,$message,$order->id,Notification::TYPE['Order'],true);
                        break;
                    }
                    default:
                        break;
                }
            }
            $order->save();
            return $this->successJsonResponse([__('messages.update_successful')], $order, 'Order');
        } else {
            return $this->failJsonResponse([__('messages.object_404')], [], 'data', [],404);
        }
    }
}
