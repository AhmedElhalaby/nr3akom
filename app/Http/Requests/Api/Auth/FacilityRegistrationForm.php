<?php

namespace App\Http\Requests\Api\Auth;

use App\Master;
use App\Models\Facilities\Facility;
use App\Models\General\DepartmentType;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Facilities\FacilityDepartments;
use App\Models\Facilities\FacilityInsurances;
use App\Models\Facilities\FacilityServices;
use App\Models\General\Insurance;
use App\Models\General\ServiceSection;
use App\Traits\ResponseTrait;

class FacilityRegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'facility_type_id' => 'required|exists:facility_types,id',
            'license_copy' => 'required|mimes:jpeg,jpg,bmp,png',
            'commercial_register_copy' => 'required|mimes:jpeg,bmp,png',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Facilities');
    }
    public function complete()
    {
        $logged = auth()->user();
        if($logged->type != 1){
            return $this->failJsonResponse([__('auth.not_facility')]);
        }
        if($logged->facility_id != null){
            return $this->failJsonResponse([__('auth.registration_completed_before')]);
        }
        $supported_insurances = explode(',', ($this->supported_insurances)?$this->supported_insurances:'');
        $services = explode(',', ($this->services)?$this->services:'');
        $facilities_departments = explode(',', ($this->facilities_departments)?$this->facilities_departments:'');

        $facility = new Facility();
        $facility->name = $this->name;
        $facility->facility_type_id = $this->facility_type_id;
        $facility->license_num = ($this->license_num)?$this->license_num:'';
        $facility->license_copy = $this->license_copy;
        $facility->commercial_register_num = ($this->commercial_register_num)?$this->commercial_register_num:'';
        $facility->commercial_register_copy = $this->commercial_register_copy;



        if ($facility->save()) {
            foreach ($supported_insurances as $supported_insurance) {
                $insurance = Insurance::where('id', $supported_insurance)->first();
                if ($insurance != null) {
                    $FacilityInsurance= new FacilityInsurances();
                    $FacilityInsurance->insurance_id = $insurance->id;
                    $FacilityInsurance->facility_id = $facility->id;
                    $FacilityInsurance->save();
                }
            }
            foreach ($services as $service) {
                $service = ServiceSection::where('id', $service)->first();
                if ($service != null) {
                    $FacilityService= new FacilityServices();
                    $FacilityService->service_section_id = $service->id;
                    $FacilityService->facility_id = $facility->id;
                    $FacilityService->save();
                }
            }
            foreach ($facilities_departments as $facilities_department) {
                $department = DepartmentType::where('id', $facilities_department)->first();
                if ($department != null) {
                    $FacilityDepartment= new FacilityDepartments();
                    $FacilityDepartment->department_type_id = $department->id;
                    $FacilityDepartment->facility_id = $facility->id;
                    $FacilityDepartment->save();
                }
            }
            $logged->facility_id = $facility->id;
            $logged->save();
        }
        return $this->successJsonResponse([__('auth.registration_completed')],$logged,'User');
    }
}
