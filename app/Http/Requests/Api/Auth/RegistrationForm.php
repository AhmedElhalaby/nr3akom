<?php

namespace App\Http\Requests\Api\Auth;

use App\Master;
use App\Models\Auth\DeviceToken;
use App\Http\Requests\Api\ApiRequest;
use App\Traits\ResponseTrait;
use App\Models\Auth\User;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class RegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'type' => ['required',
                Rule::in(User::TYPES),
            ],
            'gender' => [Rule::in([1,2])],
            'device_token' => 'string|required_with:device_type',
            'device_type' => 'string|required_with:device_token',
            'name' => 'required|string|max:255',
        ];
    }
    public function attributes()
    {
        return Master::NiceNames('User');
    }
    public function persist()
    {
        $user = new User([
            'type' => $this->type,
            'name' => $this->name ? $this->name : $this->name_ar,
            'email' => $this->email,
            'password' => Hash::make($this->password),
            'mobile' => $this->mobile,
            'gender' => $this->has('gender')?$this->gender:'m',
        ]);

        $user->save();
        if ($this->input('device_token')) {
            $user->device_token;
            $user->device_type;
            $user->save();
        }
        Auth::attempt(request(['email', 'password']));
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        $token->save();
        $user->refresh();
        $user['access_token']= $tokenResult->accessToken;
        $user['token_type']= 'Bearer';
        $user['expires_at']= Carbon::parse(
            $tokenResult->token->expires_at
        )->toDateTimeString();
        return $this->successJsonResponse( [__('auth.registration_successful')],$user,'User');

    }

}
