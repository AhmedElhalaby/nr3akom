<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Resources\Api\UserResource;
use App\Models\Facilities\FacilityDepartments;
use App\Models\General\DepartmentType;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image' => 'image',
            'gender' => [Rule::in(['m','f'])],
            'name' => 'string|max:255',
            'email' => 'string|email|max:255|unique:users,email,'.auth()->user()->id,

        ];
    }

    public function update()
    {
        $user = auth()->user();
        if ($this->hasFile('image'))
            $user->image = $this->image;
        if ($this->has('name'))
            $user->name = $this->name;
        if ($this->has('email'))
            $user->email = $this->email;
        if ($this->has('mobile'))
            $user->mobile = $this->mobile;
        if ($this->has('gender'))
            $user->gender = $this->gender;

//////////////////////////////////////////////////////////////////
        if ($user->type ==1){
            $facility = $user->facility;
            if ($this->hasFile('logo'))
                $facility->logo = $this->logo;
            if ($this->has('facility_name'))
                $facility->name = $this->facility_name;
            if ($this->has('facility_email'))
                $facility->email = $this->facility_email;
            if ($this->has('phone'))
                $facility->phone = $this->phone;
            if ($this->has('address'))
                $facility->address = $this->address;
            if ($this->hasFile('license_copy'))
                $facility->license_copy = $this->license_copy;
            if ($this->has('license_num'))
                $facility->license_num = $this->license_num;
            if ($this->hasFile('commercial_register_copy'))
                $facility->commercial_register_copy = $this->commercial_register_copy;
            if ($this->has('commercial_register_num'))
                $facility->commercial_register_num = $this->commercial_register_num;
            if ($this->has('facility_type_id'))
                $facility->facility_type_id = $this->facility_type_id;
            if ($this->has('facilities_departments')){
                $facilities_departments = explode(',', ($this->facilities_departments)?$this->facilities_departments:'');
                $Old_FD = FacilityDepartments::where('facility_id',$facility->id)->delete();
                foreach ($facilities_departments as $facilities_department) {
                    $department = DepartmentType::where('id', $facilities_department)->first();
                    if ($department != null) {
                        $FacilityDepartment= new FacilityDepartments();
                        $FacilityDepartment->department_type_id = $department->id;
                        $FacilityDepartment->facility_id = $facility->id;
                        $FacilityDepartment->save();
                    }
                }
            }
            $facility->save();
        }
        if($user->type == 2){
            $doctor= $user->doctor;
            if($this->has('birthday'))
                $doctor->birthday = $this->birthday;
            if($this->has('specialization_id'))
                $doctor->specialization_id = $this->specialization_id;
            $doctor->save();
        }
        if($user->type == 3){
            $normal_user= $user->normal_user;
            if($this->has('nationality'))
                $normal_user->nationality = $this->nationality;
            if($this->has('address'))
                $normal_user->address = $this->address;
            if($this->has('identity'))
                $normal_user->identity = $this->identity;
            if($this->has('identity_image'))
                $normal_user->identity_image = $this->identity_image;
            if($this->has('insurance_id'))
                $normal_user->insurance_id = $this->insurance_id;
            if($this->has('insurance_image'))
                $normal_user->insurance_image = $this->insurance_image;
            if($this->has('dob'))
                $normal_user->dob = $this->dob;
            if($this->has('medical_status'))
                $normal_user->medical_status = $this->medical_status;
            $normal_user->save();
        }
        $user->save();
        return $this->successJsonResponse( [__('auth.update_successful')],$user,'User');

    }
}
