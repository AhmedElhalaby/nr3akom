<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\Hash;

class PasswordRequest extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch ($this->method()) {
            case 'GET':
                {
                    return false;
                }
            case 'DELETE':
                {
                    return false;
                }
            case 'POST':
                {
                    return auth('api')->user() != null;
                }
            case 'PUT':
                {
                    return false;
                }
            case 'PATCH':
                {
                    return false;
                }
            default:
                return false;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|string|min:6',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

    public function update()
    {
        $logged = auth()->user();
        if(Hash::check($this->old_password,$logged->password)){
            $logged->password = Hash::make($this->password);
            $logged->save();
            return $this->successJsonResponse([__('messages.update_successful')], $logged,'User');
        }
        return $this->failJsonResponse([__('messages.password_not_correct')]);
    }
}
