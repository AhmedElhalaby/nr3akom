<?php

namespace App\Http\Requests\Api\Auth;

use App\Master;
use App\Models\Auth\NormalUser;
use App\Models\Facilities\Facility;
use App\Models\General\DepartmentType;
use App\Http\Requests\Api\ApiRequest;
use App\Models\Facilities\FacilityDepartments;
use App\Models\Facilities\FacilityInsurances;
use App\Models\Facilities\FacilityServices;
use App\Models\General\Insurance;
use App\Models\General\ServiceSection;
use App\Traits\ResponseTrait;

class UserRegistrationForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nationality_id' => 'required|exists:nationalities,id',
            'insurance_id' => 'required|exists:insurances,id',
            'insurance_image' => 'required|mimes:jpeg,jpg,bmp,png',
            'identity' => 'required',
            'identity_image' => 'required|mimes:jpeg,jpg,bmp,png',
            'address' => 'required|string',
            'dob' => 'required',
            'medical_status' => 'string',

        ];
    }
    public function attributes()
    {
        return Master::NiceNames('Facilities');
    }
    public function complete()
    {
        $logged = auth()->user();
        if($logged->type != 3){
            return $this->failJsonResponse([__('auth.not_normal_user')]);
        }
        if(NormalUser::where('user_id',$logged->id)->first()){

        }
        $NormalUser = new NormalUser();
        $NormalUser->user_id = $logged->id;
        $NormalUser->nationality_id = $this->nationality_id;
        $NormalUser->insurance_id = $this->insurance_id;
        $NormalUser->insurance_image = $this->insurance_image;
        $NormalUser->identity = $this->identity;
        $NormalUser->identity_image = $this->identity_image;
        $NormalUser->address = $this->address;
        $NormalUser->dob = $this->dob;
        $NormalUser->medical_status = ($this->medical_status)?$this->medical_status:'';
        $NormalUser->save();

        return $this->successJsonResponse([__('auth.registration_completed')],$logged,'User');
    }
}
