<?php

namespace App\Http\Requests\Api\Auth;

use App\Http\Requests\Api\ApiRequest;
use App\Traits\ResponseTrait;

class RefreshForm extends ApiRequest
{
    use ResponseTrait;

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'device_type' => 'required|string',
            'device_token' => 'required|string'
        ];
    }

    public function refresh()
    {
        $user = $this->user();
        $user->device_type = $this->device_type;
        $user->device_token = $this->device_token;
        $user->save();
        return $this->successJsonResponse( [__('auth.refreshed')]);

    }
}
