<?php

namespace App\Exports;

use App\Models\Auth\User;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class DoctorExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return User[]|Collection
    */
    public function collection()
    {
        return User::where('type',User::TYPES['Doctor'])->get();
    }
}
