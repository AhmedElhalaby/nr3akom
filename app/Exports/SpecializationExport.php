<?php

namespace App\Exports;

use App\Models\General\Specialization;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class SpecializationExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Specialization[]|Collection
    */
    public function collection()
    {
        return Specialization::all();
    }
}
