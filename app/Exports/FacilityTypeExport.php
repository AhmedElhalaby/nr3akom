<?php

namespace App\Exports;

use App\Models\General\FacilityType;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class FacilityTypeExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return FacilityType[]|Collection
    */
    public function collection()
    {
        return FacilityType::all();
    }
}
