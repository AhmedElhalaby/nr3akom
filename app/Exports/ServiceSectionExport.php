<?php

namespace App\Exports;

use App\Models\General\ServiceSection;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class ServiceSectionExport implements FromCollection
{

    /**
     * TargetsExport constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
    * @return ServiceSection[]|Collection
    */
    public function collection()
    {
        return ServiceSection::all();
    }
}
