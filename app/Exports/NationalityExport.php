<?php

namespace App\Exports;

use App\Models\General\Nationality;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class NationalityExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Nationality[]|Collection
    */
    public function collection()
    {
        return Nationality::all();
    }
}
