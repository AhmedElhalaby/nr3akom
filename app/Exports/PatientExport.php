<?php

namespace App\Exports;

use App\Http\Resources\Api\PatientResource;
use App\Models\Auth\User;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

/**
 * @property null type
 */
class PatientExport implements FromCollection,WithHeadings
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return AnonymousResourceCollection
    */
    public function collection()
    {
        return PatientResource::collection(User::where('type',User::TYPES['NormalUser'])->get());
    }
    public function headings(): array
    {
        return [
            __('User.name'),
            __('User.email'),
            __('User.mobile'),
            __('User.gender'),
            __('Patient.nationality_id'),
            __('Patient.address'),
            __('Patient.identity'),
            __('Patient.insurance_id'),
            __('Patient.dob'),
            __('Patient.medical_status'),
        ];
    }
}
