<?php

namespace App\Exports;

use App\Models\General\DepartmentType;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class DepartmentTypeExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return DepartmentType[]|Collection
    */
    public function collection()
    {
        return DepartmentType::all();
    }
}
