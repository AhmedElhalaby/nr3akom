<?php

namespace App\Exports;

use App\Models\General\Service;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class ServiceExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Service[]|Collection
    */
    public function collection()
    {
        return Service::all();
    }
}
