<?php

namespace App\Exports;

use App\Models\General\Insurance;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

/**
 * @property null type
 */
class InsuranceExport implements FromCollection
{
    /**
     * TargetsExport constructor.
     */
    public function __construct()
    {
        //
    }

    /**
    * @return Insurance[]|Collection
    */
    public function collection()
    {
        return Insurance::all();
    }
}
