<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Nationality extends Model
{
    protected $table ='nationalities';
    protected $fillable = ['name','ar_name'];
    protected $appends = ['final_name'];

    /*                                         *Getter*                                       */
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        else
            return $this->name;
    }
}
