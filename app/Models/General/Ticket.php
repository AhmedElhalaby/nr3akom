<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $table ='tickets';
    protected $fillable = ['type','user_id','facility_id','title','message','attachment','status'];

    /*                                         *Getter*                                       */
}
