<?php

namespace App\Models\General;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table ='notifications';
    protected $fillable = ['user_id','ref_id','title','message','type','read_at'];

    /*---------------------------------------------------------------------*/
    /*----------------------------- Functions -----------------------------*/
    /*---------------------------------------------------------------------*/
    const TYPE = [
        'General'=>0,
        'OrderChat'=>1,
        'FacilityChat'=>2,
        'Order'=>3,
        'Facility'=>4,
    ];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
}
