<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table ='services';
    protected $fillable = ['name','ar_name','description','ar_description','is_active',];
    protected $appends = ['service_section_count','ServiceSections','final_name','final_description'];

    public function service_sections(){
        return $this->hasMany('App\Models\General\ServiceSection','service_id','id');
    }

    /*                                         *Getter*                                       */
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        else
            return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        else
            return $this->description;
    }

    public function getServiceSectionCountAttribute(){
        return $this->service_sections()->count();
    }

    public function getServiceSectionsAttribute(){
        $service_sections = $this->service_sections()->get()->makeHidden(['Service']);
        return $service_sections;
    }

}
