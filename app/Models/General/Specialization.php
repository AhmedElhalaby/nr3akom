<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $table ='specializations';
    protected $fillable = ['name','ar_name','description','ar_description',];
    protected $appends = ['final_name','final_description'];

    /*                                         *Getter*                                       */
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        else
            return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        else
            return $this->description;
    }
    public function is_supported($id){
        if(SpecializationService::where('specialization_id',$this->id)->where('service_section_id',$id)->first())
            return true;
        else
            return false;
    }
}
