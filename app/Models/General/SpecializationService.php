<?php

namespace App\Models\General;

use Illuminate\Database\Eloquent\Model;

class SpecializationService extends Model
{
    protected $table ='specialization_services';
    protected $fillable = ['specialization_id','service_section_id'];
}
