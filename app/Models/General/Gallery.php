<?php

namespace App\Models\General;

use App\Models\Facilities\Facility;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table='gallery';
    protected $fillable = ['type', 'ref_id', 'image'];
    const TYPE=[
        '1',// Facilities departments pictures
    ];
    public function facility()
    {
        return $this->belongsto(Facility::class);
    }
    public function insurance()
    {
        return $this->belongsto(Insurance::class);
    }
}
