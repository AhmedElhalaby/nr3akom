<?php

namespace App\Models\General;

use App\Models\Facilities\FacilityInsurances;
use Illuminate\Database\Eloquent\Model;

class Insurance extends Model
{
    protected $table ='insurances';
    protected $fillable = ['name','ar_name','website','address','phone','phone2',];
    protected $appends = ['final_name','final_description','is_selected'];

    /*                                         *Getter*                                       */
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        else
            return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        else
            return $this->description;
    }
    public function getIsSelectedAttribute(){
        if(auth()->check()){
            $ob = FacilityInsurances::where('facility_id',auth()->user()->facility_id)->where('insurance_id',$this->id)->first();
            if($ob)
                return true;
        }
        return false;
    }
}
