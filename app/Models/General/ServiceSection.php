<?php

namespace App\Models\General;

use App\Models\Facilities\FacilityServices;
use Illuminate\Database\Eloquent\Model;

class ServiceSection extends Model
{
    protected $table ='service_sections';
    protected $fillable = ['name','ar_name','description','ar_description','service_id','is_active',];
    protected $appends = ['final_name','final_description','Service','is_selected'];

    public function service()
    {
        return $this->belongsto(Service::class);
    }
    /*                                         *Getter*                                       */
    public function getFinalNameAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_name;
        else
            return $this->name;
    }
    public function getFinalDescriptionAttribute(){
        if(app()->getLocale() == 'ar')
            return $this->ar_description;
        else
            return $this->description;
    }
    public function getServiceAttribute(){
        $service = Service::find($this->service_id);
        $service->setAppends(['final_name','final_description']);
        return $service;
    }
    public function getIsSelectedAttribute(){
        if(auth()->check()){
            $ob = FacilityServices::where('facility_id',auth()->user()->facility_id)->where('service_section_id',$this->id)->first();
            if($ob)
                return true;
        }
        return false;
    }
}
