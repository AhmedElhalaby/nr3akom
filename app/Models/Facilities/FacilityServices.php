<?php

namespace App\Models\Facilities;

use App\Models\General\ServiceSection;
use Illuminate\Database\Eloquent\Model;

class FacilityServices extends Model
{
    protected $table='facility_services';
    protected $fillable = ['facility_id','service_section_id','price'];

    public function facility()
    {
        return $this->belongsto(Facility::class);
    }
    public function service()
    {
        return $this->belongsto(ServiceSection::class);
    }
}
