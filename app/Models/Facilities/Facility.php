<?php

namespace App\Models\Facilities;

use App\Models\Auth\User;
use App\Models\General\DepartmentType;
use App\Models\General\FacilityType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class Facility extends Model
{
    protected $table='facilities';
    protected $fillable = ['facility_type_id', 'name', 'website', 'email', 'phone', 'phone2', 'address', 'location', 'lat', 'lng', 'logo', 'license_copy', 'commercial_register_copy', 'civil_defense_copy', 'civil_defense_map', 'facility_map', 'gd_name', 'gd_email', 'gd_phone', 'gd_phone2', 'md_name', 'md_email', 'md_phone', 'md_phone2', 'start_time', 'end_time', 'em_start_time', 'em_end_time'];
    protected $appends = ['facility_type_str','DepartmentType','FacilityService','user_id'];

    public function user()
    {
        return $this->belongsto(User::class,'id','facility_id')->where('type',1);
    }
    public function facility_type()
    {
        return $this->belongsto(FacilityType::class,'facility_type_id','id');
    }
    public function facility_departments()
    {
        return $this->hasMany(FacilityDepartments::class,'facility_id','id');
    }
//    ////////////////////////////////////////////////////////////////////////////////      //
    public function setLicenseCopyAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "license_copy";
        $disk = "storage";
        $destination_path = "public/facilities/license_copy/";

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }
    public function setCommercialRegisterCopyAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "commercial_register_copy";
        $disk = "storage";
        $destination_path = "public/facilities/commercial_register_copy/";

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }
    public function setLogoAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "logo";
        $disk = "storage";
        $destination_path = "public/facilities/logo/";

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }
    public function getFacilityTypeStrAttribute(){
        $facility_type = $this->facility_type()->first();
        return $facility_type->name;
    }
    public function getUserIdAttribute(){
        $user = $this->user()->first();
        return @$user->id;
    }
    public function getFacilityServiceAttribute(){
        $request = \Request::instance();
        if($request->has('service_section_id')){
            return FacilityServices::where('service_section_id',$request->service_section_id)->where('facility_id',$this->id)->first();
        }
        else
            return null;
    }
    public function getDepartmentTypeAttribute(){
        return DepartmentType::whereIn('id',$this->facility_departments()->pluck('department_type_id'))->get();
    }

}
