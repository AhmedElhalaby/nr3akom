<?php

namespace App\Models\Facilities;

use App\Models\General\Insurance;
use Illuminate\Database\Eloquent\Model;

class FacilityInsurances extends Model
{
    protected $table='facility_insurances';
    protected $fillable = ['facility_id','insurance_id'];

    public function facility()
    {
        return $this->belongsto(Facility::class,'facility_id','id');
    }
    public function insurance()
    {
        return $this->belongsto(Insurance::class,'insurance_id','id');
    }
}
