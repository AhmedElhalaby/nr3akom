<?php

namespace App\Models\Facilities;

use Illuminate\Database\Eloquent\Model;

class FacilityDepartments extends Model
{
    protected $table='facility_departments';
    protected $fillable = ['facility_id', 'department_type_id', 'location'];


    public function facility()
    {
        return $this->belongsto(Facility::class);
    }
}
