<?php

namespace App\Models\Orders;

use App\Models\Auth\User;
use App\Models\Facilities\Facility;
use App\Models\General\Gallery;
use App\Models\General\ServiceSection;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='orders';
    protected $fillable = ['user_id', 'facility_id', 'service_section_id', 'doctor_id', 'order_date', 'order_time', 'description', 'lat', 'lng', 'payment_method', 'price', 'delivery_method', 'delivery_cost','delivery_duration', 'approximate_time', 'instructions', 'order_state', 'status', 'message_status'];
    protected $appends = ['User','ServiceSection','attachment','doctor_name','Facility'];
    const Status = [
        'New'=>0,// New Order
        'Accepted'=>1,// Accepted Order
        'Finished'=>2,// Finished Order
        'Rejected'=>3,// Rejected Order
        'Cancel'=>4,// Cancel Order
    ];
    const OrderState = [
        'InitValue'=>1,
        'PharmacyReply'=>2,
        'UserConfirm'=>3,
    ];
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('created_at', 'desc');
        });
    }
    public function user()
    {
        return $this->belongsto(User::class,'user_id','id');
    }
    public function facility()
    {
        return $this->belongsto(Facility::class,'facility_id','id');
    }
    public function service_section()
    {
        return $this->belongsto(ServiceSection::class,'service_section_id','id');
    }
    public function doctor()
    {
        return $this->belongsto(User::class,'doctor_id','id');
    }
//    ////////////////////////////////////////////////////////////////////////////////      //

    public function getServiceSectionAttribute(){
        return $this->service_section()->first();
    }
    public function getUserAttribute(){
        return $this->user()->first();
    }
    public function getFacilityAttribute(){
        return $this->facility()->first();
    }
    public function getDoctorNameAttribute(){
        $doctor = $this->doctor()->first();
        if($doctor)
            return $doctor->name;
        else
            return null;
    }
    public function getAttachmentAttribute(){
        $gallery = Gallery::where('ref_id',$this->id)->where('type',6)->pluck('image');
        return $gallery;
    }
}
