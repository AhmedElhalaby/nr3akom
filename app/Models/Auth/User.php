<?php

namespace App\Models\Auth;

use App\Models\Facilities\Facility;
use App\Models\Orders\Order;
use App\Traits\RandomUniqueKey;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable,HasApiTokens,RandomUniqueKey;


    protected $table = 'users';
    protected $fillable = ['name', 'email', 'password','mobile','type','gender','image','facility_id','device_token','device_type','is_active'];
    protected $hidden = ['password', 'remember_token'];
    protected $appends = ['gender_str','type_str','Facility','Doctor','NormalUser','orders_count'];

    const TYPES = [
        'Facility'=>1,
        'Doctor'=>2,
        'NormalUser'=>3
    ];
    const GENDER = [
        'Male'=>1,
        'Female'=>2
    ];
    /*---------------------------------------------------------------------*/
    /*----------------------------- Relations -----------------------------*/
    /*---------------------------------------------------------------------*/
    public function facility()
    {
        return $this->belongsTo(Facility::class,'facility_id','id');
    }
    public function doctor()
    {
        return $this->hasOne(Doctor::class,'user_id','id');
    }
    public function normal_user()
    {
        return $this->hasOne(NormalUser::class,'user_id','id');
    }
    /*---------------------------------------------------------------------*/
    /*----------------------------- Functions -----------------------------*/
    /*---------------------------------------------------------------------*/

    protected static function boot()
    {
        parent::boot();
        static::deleting(function($object) {
            if($object->type == 2 ){
                $doctor = Doctor::where('user_id',$object->id)->first();
                $doctor->delete();
            }
        });
    }

    /*---------------------------------------------------------------------*/
    /*------------------------------ Getters ------------------------------*/
    /*---------------------------------------------------------------------*/

    public function getTypeStrAttribute(){
        if($this->type== 1)
            return __('auth.type.facility');
        elseif ($this->type == 2)
            return __('auth.type.doctor');
        elseif ($this->type == 3)
            return __('auth.type.user');
        else
            return __('auth.type.not_found');
    }
    public function getGenderStrAttribute(){
        if($this->gender== 1)
            return __('User.Gender.male');
        elseif ($this->type == 2)
            return __('User.Gender.female');
        else
            return '-';
    }
    public function getFacilityAttribute(){
        if($this->facility_id!= null)
            return $this->facility()->first();
        else
            return null;
    }
    public function getDoctorAttribute(){
        if($this->type == 2)
            return Doctor::where('user_id',$this->id)->first();
        else
            return null;
    }
    public function getNormalUserAttribute(){
        if($this->type == 3)
            return NormalUser::where('user_id',$this->id)->first();
        else
            return null;
    }
    public function getOrdersCountAttribute(){
        if($this->type == 1)
            return Order::where('facility_id',$this->facility_id)->where('status',0)->count();
        elseif($this->type == 2)
            return Order::where('doctor_id',$this->id)->where('status',1)->count();
        elseif($this->type == 3)
            return Order::where('user_id',$this->id)->where('status',1)->count();
        else
            return 0;
    }

    /*---------------------------------------------------------------------*/
    /*------------------------------ Setters ------------------------------*/
    /*---------------------------------------------------------------------*/

    public function setImageAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "image";
        $disk = "storage";
        $destination_path = "public/users/image/";

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }

}
