<?php

namespace App\Models\Auth;

use App\Models\General\Specialization;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{


    protected $table = 'doctors';
    protected $fillable = ['user_id', 'nationality_id', 'address', 'national_address', 'identity', 'identity_image', 'specialization_id', 'c_schs', 'prc_schs', 'license', 'phone', 'phone2', 'dob', 'details'];


    /*---------------------------------------------------------------------*/
    /*----------------------------- Relations -----------------------------*/
    /*---------------------------------------------------------------------*/
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function specialization()
    {
        return $this->belongsTo(Specialization::class,'specialization_id','id');
    }
    /*---------------------------------------------------------------------*/
    /*----------------------------- Functions -----------------------------*/
    /*---------------------------------------------------------------------*/

}
