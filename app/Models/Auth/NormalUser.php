<?php

namespace App\Models\Auth;

use App\Models\General\Insurance;
use App\Models\General\Nationality;
use Illuminate\Database\Eloquent\Model;

class NormalUser extends Model
{


    protected $table = 'normal_users';
    protected $fillable = ['user_id',
        'nationality_id',
        'address',
        'identity',
        'identity_image',
        'insurance_id',
        'insurance_image',
        'dob',
        'medical_status',
        ];


    /*---------------------------------------------------------------------*/
    /*----------------------------- Relations -----------------------------*/
    /*---------------------------------------------------------------------*/
    public function user()
    {
        return $this->belongsTo(User::class,'user_id','id');
    }
    public function nationality()
    {
        return $this->belongsTo(Nationality::class,'nationality_id','id');
    }
    public function insurance()
    {
        return $this->belongsTo(Insurance::class,'insurance_id','id');
    }
    /*---------------------------------------------------------------------*/
    /*----------------------------- Functions -----------------------------*/
    /*---------------------------------------------------------------------*/

    public function setIdentityImageAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "identity_image";
        $disk = "storage";
        $destination_path = "public/users/identity_image/";

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }
    public function setInsuranceImageAttribute($value)
    {
        $request = \Request::instance();
        $attribute_name = "insurance_image";
        $disk = "storage";
        $destination_path = "public/users/insurance_image/";

        // if a new file is uploaded, store it on disk and its filename in the database
        if ($request->hasFile($attribute_name) && $request->file($attribute_name)->isValid()) {
            // 1. Generate a new file name
            $file = $request->file($attribute_name);
            $new_file_name = md5($file->getClientOriginalName().time()).'.'.$file->getClientOriginalExtension();
            // 2. Move the new file to the correct path
            $file_path = $file->move($destination_path, $new_file_name);
            // 3. Save the complete path to the database
            $this->attributes[$attribute_name] = $destination_path.$new_file_name;
        }
    }
}
