<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth',
    'namespace'  => 'Api',
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@register');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::post('facility_registration','AuthController@facility_registration');
        Route::post('user_registration','AuthController@user_registration');
        Route::get('me', 'AuthController@show');
        Route::post('update', 'AuthController@update');
        Route::post('change_password', 'AuthController@change_password');
        Route::post('refresh', 'AuthController@refresh');
        Route::post('logout', 'AuthController@logout');
    });
});

Route::group([
    'namespace'  => 'Api',
], function () {
    Route::get('install', 'HomeController@install');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('patients', 'HomeController@patients');

        Route::group([
            ///// Orders
        ],function (){
            //          Orders Lists & Modifications & Replies
            Route::get('orders', 'OrderController@index');
            Route::get('orders/{id}', 'OrderController@show');
            Route::post('orders/{id}', 'OrderController@update');
            Route::post('orders', 'OrderController@store');
            Route::post('orders/pharmacy_response', 'OrderController@pharmacy_response');
            Route::get('order/facilities', 'OrderController@facilities');
            Route::get('order/facilities/doctors', 'OrderController@doctors');

        });
        Route::group([
            'namespace'  => 'Facilities',
            'prefix'  => 'facility',
        ], function() {
//          Doctors Lists & Modifications & Creation
            Route::get('doctors', 'DoctorController@index');
            Route::post('doctors', 'DoctorController@store');
            Route::get('doctors/{id}', 'DoctorController@show');
            Route::post('doctors/{id}', 'DoctorController@update');
            Route::delete('doctors/{id}', 'DoctorController@destroy');
//          Insurances Lists & Modifications
            Route::get('insurances', 'InsuranceController@index');
            Route::post('insurances', 'InsuranceController@update');
//          Services Lists & Modifications
            Route::get('services', 'ServiceController@index');
            Route::post('services', 'ServiceController@update');

        });

        Route::group([
            ///// Message
        ],function (){
            //          Orders Lists & Modifications & Replies
            Route::post('messages/{id}', 'HomeController@message');

        });
        Route::group([
            'prefix' => 'notifications',
        ], function() {
            Route::get('/', 'NotificationController@index');
            Route::post('/read/{id}', 'NotificationController@read');
            Route::post('/read_all', 'NotificationController@read_all');
        });
    });
});
