<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'HomeController@index');
Route::get('app/lang', 'HomeController@lang');
Route::get('privacy', 'HomeController@privacy');
/*
|--------------------------------------------------------------------------
| Web Auth
|--------------------------------------------------------------------------
| Here is where Web auth routes exists for login and log out
*/
Route::group([
    'namespace'  => 'Facility\Auth',
], function() {
    Route::get('login', ['uses' => 'LoginController@showLoginForm','as'=>'login']);
    Route::post('login', ['uses' => 'LoginController@login']);
    Route::get('register', 'RegisterController@showRegistrationForm');
    Route::post('register', 'RegisterController@register');

    Route::group([
//        'middleware' => 'auth',
    ], function() {
        Route::get('facility/register/complete', 'CompleteRegisterController@showCompleteRegistrationForm');
        Route::post('facility/register/complete', 'CompleteRegisterController@CompleteRegister');

        Route::post('logout', ['uses' => 'LoginController@logout','as'=>'facility.logout']);
    });
});
Route::group([
    'middleware' => ['auth','registration.complete'],
    'prefix' => 'facility',
    'namespace'  => 'Facility',
], function() {
    Route::get('/', 'HomeController@index');
    Route::post('notification', 'HomeController@notification');
    Route::resource('doctors', 'DoctorController');
    Route::get('patients', 'HomeController@patients');
    Route::patch('doctors/update/password',  'DoctorController@updatePassword');
    Route::post('doctors/{id}/notification',  'DoctorController@notification');

    Route::resource('orders', 'OrderController')->only(['index','show','update','edit']);
    Route::post('orders/{id}/notification',  'OrderController@notification');

    Route::get('profile', 'HomeController@profile');
    Route::post('profile', 'HomeController@update_profile');


});


