<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/

Route::group([
    'namespace'  => 'Admin',
], function() {

    /*
    |--------------------------------------------------------------------------
    | Admin Auth
    |--------------------------------------------------------------------------
    | Here is where admin auth routes exists for login and log out
    */
    Route::group([
        'namespace'  => 'Auth',
    ], function() {
        Route::get('login', ['uses' => 'LoginController@showLoginForm','as'=>'admin.login']);
        Route::post('login', ['uses' => 'LoginController@login']);
        Route::group([
            'middleware' => 'App\Http\Middleware\RedirectIfAuthenticatedAdmin',
        ], function() {
            Route::post('logout', ['uses' => 'LoginController@logout','as'=>'admin.logout']);
        });
    });
    /*
    |--------------------------------------------------------------------------
    | Admin After login in
    |--------------------------------------------------------------------------
    | Here is where admin panel routes exists after login in
    */
    Route::group([
        'middleware'  => ['App\Http\Middleware\RedirectIfAuthenticatedAdmin'],
    ], function() {
        Route::get('/', 'HomeController@index');
        Route::post('notification/send', 'HomeController@general_notification');

        /*
        |--------------------------------------------------------------------------
        | Admin > App Managemnet
        |--------------------------------------------------------------------------
        | Here is where App Managemnet routes
        */
        Route::group([
            'namespace'=>'AppManagement',
            'prefix'=>'app_managements'
        ],function () {
            Route::resource('admins','AdminController');
            Route::patch('admins/update/password',  'AdminController@updatePassword');
            Route::get('admins/option/export','AdminController@export');
            Route::get('admins/{id}/activation','AdminController@activation');
        });
        /*
        |--------------------------------------------------------------------------
        | Admin > App Setting
        |--------------------------------------------------------------------------
        | Here is where App Setting routes
        */
        Route::group([
            'namespace'=>'AppSetting',
            'prefix'=>'app_settings'
        ],function (){
            Route::resource('facility_types','FacilityTypeController');
            Route::get('facility_types/option/export','FacilityTypeController@export');

            Route::resource('department_types','DepartmentTypeController');
            Route::get('department_types/option/export','DepartmentTypeController@export');

            Route::resource('specializations','SpecializationController');
            Route::get('specializations/option/export','SpecializationController@export');

            Route::resource('insurances','InsuranceController');
            Route::get('insurances/option/export','InsuranceController@export');

            Route::resource('nationalities','NationalityController');
            Route::get('nationalities/option/export','NationalityController@export');

            Route::resource('services','ServiceController');
            Route::get('services/option/export','ServiceController@export');
            Route::get('services/{id}/activation','ServiceController@activation');

            Route::resource('services/{id}/service_sections','ServiceSectionController');
            Route::get('services/{id}/service_sections/option/export','ServiceSectionController@export');
            Route::get('services/{id}/service_sections/{id2}/activation','ServiceSectionController@activation');
        });
        /*
        |--------------------------------------------------------------------------
        | Admin > App User
        |--------------------------------------------------------------------------
        | Here is where App User routes
        */
        Route::group([
            'namespace'=>'AppUser',
            'prefix'=>'app_users'
        ],function () {
            Route::resource('patients','PatientController');
            Route::patch('patients/update/password',  'PatientController@updatePassword');
            Route::get('patients/option/export','PatientController@export');
            Route::get('patients/{id}/activation','PatientController@activation');

            Route::resource('doctors','DoctorController')->only('index','show','edit','destroy');
            Route::patch('doctors/update/password',  'DoctorController@updatePassword');
            Route::get('doctors/option/export','DoctorController@export');
            Route::get('doctors/{id}/activation','DoctorController@activation');

        });
    });
});

