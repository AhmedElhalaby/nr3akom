<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('facility_type_id');
            $table->string('name');
            $table->string('website')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->string('address')->nullable();
            $table->string('location')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('logo')->nullable();
            $table->string('license_copy')->nullable();
            $table->string('commercial_register_copy')->nullable();
            $table->string('license_num')->nullable();
            $table->string('commercial_register_num')->nullable();
            $table->string('civil_defense_copy')->nullable();
            $table->string('civil_defense_map')->nullable();
            $table->string('facility_map')->nullable();
            $table->string('gd_name')->nullable();
            $table->string('gd_email')->nullable();
            $table->string('gd_phone')->nullable();
            $table->string('gd_phone2')->nullable();
            $table->string('md_name')->nullable();
            $table->string('md_email')->nullable();
            $table->string('md_phone')->nullable();
            $table->string('md_phone2')->nullable();
            $table->time('start_time')->nullable();
            $table->time('end_time')->nullable();
            $table->time('em_start_time')->nullable();
            $table->time('em_end_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('facilities');
    }
}
