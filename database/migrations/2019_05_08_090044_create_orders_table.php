<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('facility_id');
            $table->unsignedBigInteger('service_section_id');
            $table->unsignedBigInteger('doctor_id')->nullable();
            $table->date('order_date')->nullable();
            $table->string('order_time_from')->nullable();
            $table->string('order_time_to')->nullable();
            $table->string('description')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->tinyInteger('payment_method')->default(1);
            $table->double('price')->nullable();
            $table->tinyInteger('delivery_method')->default(1);
            $table->double('delivery_cost')->nullable();
            $table->string('delivery_duration')->nullable();
            $table->time('approximate_time')->nullable();
            $table->string('instructions')->nullable();
            $table->tinyInteger('order_state')->default(1);
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('message_status')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
