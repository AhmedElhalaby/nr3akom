<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('user_id');
            $table->string('nationality_id')->nullable();
            $table->string('address')->nullable();
            $table->string('national_address')->nullable();
            $table->string('identity_num')->nullable();
            $table->string('identity_image')->nullable();
            $table->string('specialization_id')->nullable();
            $table->string('c_schs')->nullable();
            $table->string('prc_schs')->nullable();
            $table->string('license')->nullable();
            $table->string('phone')->nullable();
            $table->string('phone2')->nullable();
            $table->date('dob')->nullable();
            $table->string('details')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
