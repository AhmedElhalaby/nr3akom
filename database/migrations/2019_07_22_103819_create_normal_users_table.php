<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNormalUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('normal_users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('nationality_id')->nullable();
            $table->string('address')->nullable();
            $table->string('identity')->nullable();
            $table->string('identity_image')->nullable();
            $table->unsignedBigInteger('insurance_id')->nullable();
            $table->string('insurance_image')->nullable();
            $table->date('dob')->nullable();
            $table->string('medical_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('normal_users');
    }
}
