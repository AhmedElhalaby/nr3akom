@extends('facility.layout.app')
@section('content')
    <div class="container">
        <div class="row p-0 m-0">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header p-2">
                        <span>{{__('facility.edit')}} {{__('Doctor.the_doctor')}}</span>
                    </div>
                    <div class="panel-body max-vh-75">
                        <div class="container">
                            <form action="{{url('facility/doctors/'.$Object->id)}}" method="post" class="row pt-4 px-4" enctype="multipart/form-data">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">
                                <div class="col-md-4 form-group text-center">
                                    <label for="name">{{__('User.name')}}</label>
                                    <input type="text" name="name" id="name" class="form-control login-input" value="{{$Object->name}}" required>
                                </div>
                                <div class="col-md-4 form-group text-center">
                                    <label for="email">{{__('User.email')}}</label>
                                    <input type="email" name="email" id="email" class="form-control login-input" value="{{$Object->email}}" required>
                                </div>
                                <div class="col-md-4 form-group text-center">
                                    <label for="mobile">{{__('User.mobile')}}</label>
                                    <input type="tel" name="mobile" id="mobile" class="form-control login-input" value="{{$Object->mobile}}" required>
                                </div>
                                <div class="col-md-4 form-group text-center">
                                    <label for="specialization_id">{{__('Doctor.specialization_id')}}</label>
                                    <select name="specialization_id" id="specialization_id" class="form-control login-select" required>
                                        @foreach(App\Models\General\Specialization::all() as $Specialization)
                                            <option value="{{$Specialization->id}}" @if($Object->doctor->specialization_id == $Specialization->id) selected @endif>{{$Specialization->final_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4 form-group text-center">
                                    <label for="gender">{{__('User.gender')}}</label>
                                    <select name="gender" id="gender" class="form-control login-select" required>
                                        <option value="1" @if($Object->gender == 1) selected @endif>{{__('auth.gender.f')}}</option>
                                        <option value="2" @if($Object->gender == 2) selected @endif>{{__('auth.gender.m')}}</option>
                                    </select>
                                </div>

                                <div class="col-md-4 form-group text-center">
                                    <label for="birthday">{{__('Doctor.birthday')}}</label>
                                    <input type="date" name="birthday" id="birthday" class="form-control login-input" value="{{$Object->doctor->birthday}}" required>
                                </div>

                                <div class="col-md-4 form-group text-center">
                                    <label for="nationality_id">{{__('Doctor.nationality_id')}}</label>
                                    <select name="nationality_id" id="nationality_id" class="form-control login-select" required>
                                        @foreach(App\Models\General\Nationality::all() as $Nationality)
                                            <option value="{{$Nationality->id}}" @if($Object->doctor->nationality_id == $Nationality->id) selected @endif>{{$Nationality->final_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
{{--                                <div class="form-group col-md-4 text-center">--}}
{{--                                    <label for="image">{{__('User.image')}}</label>--}}
{{--                                    <input type="button" class="form-control  btn btn-primary bg-primary btn-block login-btn " onclick="document.getElementById('image').click()" value="{{__('رفع صورة')}}">--}}
{{--                                    <input type="file" class="hidden" name="image" id="image">--}}
{{--                                </div>--}}

                                <div class="col-md-4 form-group text-center">
                                    <label for="address">{{__('Doctor.address')}}</label>
                                    <input type="text" name="address" id="address" class="form-control login-input" value="{{$Object->doctor->address}}" required>
                                </div>
                                <div class="col-md-4 form-group text-center">
                                    <label for="identity_num">{{__('Doctor.identity_num')}}</label>
                                    <input type="text" name="identity_num" id="identity_num" class="form-control login-input" value="{{$Object->doctor->identity_num}}" required>
                                </div>
                                <div class="col-md-4 form-group text-center">
                                    <label for="national_address">{{__('Doctor.national_address')}}</label>
                                    <input type="text" name="national_address" id="national_address" class="form-control login-input" value="{{$Object->doctor->national_address}}" required>
                                </div>
                                <div class="col-md-12 form-group text-center">
                                    <label for="details">{{__('Doctor.details')}}</label>
                                    <textarea name="details" id="details" class="form-control login-input" required>{{$Object->doctor->details}}</textarea>
                                </div>
                                <br>
                                <div class="form-group col-md-12 text-center">
                                    <label for=""></label>
                                    <input type="submit" name="submit" class="btn btn-block btn-primary btn-block login-btn" value="{{__('facility.save')}}">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
