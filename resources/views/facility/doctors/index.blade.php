@extends('facility.layout.app')
@section('content')
    <div class="container">
        <div class="row p-0 m-0">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header p-2">
                        <div class="pagination-div  d-inline-block">
                            {{ $Objects->links() }}
                        </div>
                        <span>{{__('Doctor.doctors')}}</span>
                        <a href="{{url('facility/doctors/create')}}" class="add-link color-white" data-toggle="tooltip" title="{{__('facility.add')}}"><i class=" fa fa-plus-circle mt-1"></i></a>
                    </div>
                    <div class="panel-body max-vh-75">
                        @foreach($Objects as $Object)
                            <div class="row p-0 m-0 border-bottom item-hover">
                                <div class="col-md-6">
                                    <div class="row p-0 m-0">
                                        <div class="col-md-3 align-self-center">
                                            <img src="{{asset((file_exists($Object->image))?$Object->image:'public/users/image/default.jpg')}}" class="border-radius-50 bg-white" width="100" height="100" alt="">
                                        </div>
                                        <div class="col-md-9 mt-1">
                                            <span class="d-block p-2"><i class="fa fa-user float-left mt-1" data-toggle="tooltip" data-title="{{__('User.name')}}"></i> <span class="px-3">{{$Object->name}}</span></span>
                                            <span class="d-block p-2"><i class="fa fa-location-arrow float-left mt-1" data-toggle="tooltip" data-title="{{__('Doctor.address')}}"></i> <span class="px-3">{{$Object->doctor->address}}  </span></span>
                                            <span class="d-block p-2"><i class="fa fa-suitcase float-left mt-1" data-toggle="tooltip" data-title="{{__('Doctor.specialization_id')}}"></i> <span class="px-3">{{$Object->doctor->specialization->final_name}}</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mt-1">
                                    <span class="d-block p-2"><i class="fa fa-bars float-left mt-1" data-toggle="tooltip" data-title="{{__('Doctor.details')}}"></i> <span class="px-3">{{$Object->doctor->details}}  </span></span>
                                </div>
                                <div class="col-md-2 pt-4">
                                    <a href="{{url('facility/orders?doctor='.$Object->id)}}" data-toggle="tooltip" title="{{__('facility.sidebar.orders')}}" class="text-success"><i class="fa fa-clipboard-list"></i></a>
                                    <a href="{{url('facility/doctors/'.$Object->id.'/edit')}}" data-toggle="tooltip" title="{{__('facility.edit')}}" class="text-success"><i class="fa fa-edit"></i></a>
                                    <a href="#" class="fs-20" data-toggle="modal" data-target="#EditPassword" onclick="document.getElementById('UserName').innerHTML = '{{$Object->name}}';document.getElementById('user_id').value = '{{$Object->id}}'"><i class="fa fa-key" data-toggle="tooltip" data-placement="bottom" title="{{__('facility.change_password')}}"></i></a>
                                    <a href="#" class="fs-20 text-danger" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$Object->name}}';document.getElementById('id').value = '{{$Object->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('facility.delete')}}"></i></a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="EditPassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{url('facility/doctors/update/password')}}" class="w-100" method="post">
                <input name="_method" type="hidden" value="PATCH">
                @csrf
                <div class="modal-content text-center">
                    <div class="modal-body p-4">
                        <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                        <input type="hidden" name="id" id="user_id" >
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="password" class="control-label">{{__('Admin.password')}} *</label>
                                    <input type="password" id="password" name="password" required class="form-control login-input {{ $errors->has('password') ? ' is-invalid' : '' }}" >
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="password_confirmation" class="control-label">{{__('Admin.password_confirmation')}} *</label>
                                    <input type="password" id="password_confirmation" name="password_confirmation" required class="form-control login-input {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" >
                                </div>
                                @if ($errors->has('password_confirmation'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block login-btn">{{__('admin.save')}}</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <form action="{{url('facility/doctors/destroy')}}" class="w-100" method="post">
                <input name="_method" type="hidden" value="DELETE">
                @csrf
                <div class="modal-content text-center">
                    <div class="modal-body p-4">
                        <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>

                        <input type="hidden" name="id" id="id" >
                        <br>
                        <h5>{{__('admin.sure_to_delete')}} {{__('Doctor.the_doctor')}} !! </h5>
                        <br>
                        <button type="submit" class="btn btn-danger btn-block nr3akom-btn">{{__('admin.delete')}}</button>

                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
