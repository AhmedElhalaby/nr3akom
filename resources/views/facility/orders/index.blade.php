@extends('facility.layout.app')
@section('content')
    <div class="container">
        <div class="row p-0 m-0">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header p-2">
                        <div class="pagination-div  d-inline-block">
                            {{ $Objects->links() }}
                        </div>
                        <span>{{__('Order.orders')}} @if($user_name != '0') - {{$user_name}} @endif</span>
                    </div>
                    <div class="panel-body max-vh-75">
                        @foreach($Objects as $Object)
                            <div class="row p-0 m-0 border-bottom item-hover" data-toggle="tooltip" title="{{__('facility.click_show_details')}}" onclick="window.location='{{url('facility/orders/'.$Object->id)}}'">
                                <div class="col-md-6">
                                    <div class="row p-0 m-0">
                                        <div class="col-md-3 p-3">
                                            <img src="{{asset((file_exists($Object->user->image))?$Object->user->image:'public/users/image/default.jpg')}}" class="border-radius-50" width="100" height="100" alt="">
                                        </div>
                                        <div class="col-md-9 mt-1">
                                            <span class="d-block p-2"><i class="fa fa-user float-left mt-1"></i> <span class="px-3">{{$Object->user->name}}</span></span>
                                            <span class="d-block p-2"><i class="fa fa-clipboard-list float-left mt-1"></i> <span class="px-3">{{$Object->service_section->final_name .'-'.$Object->service_section->service->final_name}} </span></span>
                                            <span class="d-block p-2"><i class="fa fa-calendar float-left mt-1"></i> <span class="px-3">{{$Object->order_date}}</span></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 mt-1">
                                    <span class="d-block p-2"><i class="fa fa-bars float-left mt-1"></i><span class="px-3 text-justify">{{$Object->description}}</span></span>
                                </div>
                                <div class="col-md-2 text-center align-self-center">
                                    <p class="badge status-bg-{{$Object->status}}">{{__('Order.statuses.'.$Object->status)}}</p>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
