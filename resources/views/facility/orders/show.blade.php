@extends('facility.layout.app')
@section('content')
    <div class="container">
        <div class="row px-5 mb-3">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header p-2">
                        <span>{{__('Order.order_details')}} #{{$Object->id}}</span>
                    </div>
                    <div class="panel-body max-vh-75">
                        <div class="container">
                            <div class="row p-0 m-0 mt-4">
                                <div class="col-md-5">

                                    <p>
                                        <i class="fa fa-clipboard-list" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.service')}}"></i>
                                        <span class="px-2">{{$Object->service_section->final_name .' - '.$Object->service_section->service->final_name}}</span>
                                    </p>
                                    <p>
                                        <i class="fa fa-calendar" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.order_date')}}"></i>
                                        <span class="px-2">{{($Object->order_date)?$Object->order_date:__('facility.none')}}</span>
                                    </p>
                                    <p>
                                        <i class="fa fa-clock" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.order_time')}}"></i>
                                        <span class="px-2">{{($Object->order_time_from)?$Object->order_time_from .' - '.$Object->order_time_to:__('facility.none')}}</span>
                                    </p>
                                    <p>
                                        <i class="fa fa-file-invoice-dollar" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.price')}}"></i>
                                        <span class="px-2">{{($Object->price)?$Object->price:__('facility.none')}}</span>
                                    </p>
                                    <p>
                                        <i class="fa fa-user-md" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Doctor.the_doctor')}}"></i>
                                        <span class="px-2">{{($Object->doctor)?$Object->doctor->name:__('facility.none')}}</span>
                                    </p>
                                    <p>
                                        <i class="fa fa-tags" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.status')}}"></i>
                                        <span class="px-2">
                                            <span class="badge status-bg-{{$Object->status}}">{{__('Order.statuses.'.$Object->status)}}</span>
                                            @if($Object->order_state == \App\Models\Orders\Order::OrderState['InitValue'] && $Object->service_section->service_id ==6)
                                                , <span class="badge badge-warning">{{__('Order.pharmacy_response')}}</span>
                                            @endif
                                        </span>
                                    </p>
                                    <p>
                                        <i class="fa fa-bars" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.description')}}"></i>
                                        <span class="px-2">{{$Object->description}}</span>
                                    </p>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-md-12">
                                        <img src="{{asset((file_exists($Object->user->image))?$Object->user->image:'public/users/image/default.jpg')}}" alt="" class="border-radius-50 mb-3" width="100" height="100">
                                    </div>
                                    <p>
                                        <i class="fa fa-user" data-toggle="tooltip" data-placement="bottom" data-title="{{__('Order.patient_name')}}"></i>
                                        <span class="px-2">{{$Object->user->name}}</span>
                                    </p>
                                    <a  href="#attachment" @if(count($Object->attachment)>0) data-toggle="modal" data-target="#attachment" @endif class="btn btn-primary bg-primary login-btn"><i class="fa fa-search-location"></i>@if(count($Object->attachment)>0) {{__('Order.attachments')}} @else {{__('Order.no_attachments')}} @endif</a>
                                </div>
                                <div class="col-md-4 text-center align-self-center">
                                    @if($Object->status == \App\Models\Orders\Order::Status['New'])
                                        <a href="#accept" data-toggle="modal" data-target="#accept" class="btn btn-success border-radius-20"> {{__('facility.accept')}}</a>
                                        <a href="#reject" data-toggle="modal" data-target="#reject" class="btn btn-danger border-radius-20"> {{__('facility.reject')}}</a>
                                    @endif
                                    @if($Object->status == \App\Models\Orders\Order::Status['Accepted'])
                                            @if($Object->order_state == \App\Models\Orders\Order::OrderState['InitValue'] && $Object->service_section->service_id ==6)
                                                <a href="#pharmacy_response" data-toggle="modal" data-target="#pharmacy_response" class="btn btn-warning border-radius-20"> {{__('facility.pharmacy_response')}}</a>
                                            @endif
                                            <a href="#finish" data-toggle="modal" data-target="#finish" class="btn btn-dark border-radius-20"> {{__('facility.finish')}}</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row px-5 pt-2">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header p-2">
                        <span>{{__('facility.send_notification')}} # {{$Object->user->name}}</span>
                    </div>
                    <div class="panel-body max-vh-75">
                        <div class="container">
                            <form action="{{url('facility/orders/'.$Object->id.'/notification')}}" class="row p-0 m-0 mt-4" method="post">
                                @csrf
                                <div class="col-md-4">
                                    <div class="form-group label-floating">
                                        <label for="title" class="control-label">{{__('facility.title')}} *</label>
                                        <input type="text" name="title" id="title" data-live-search="true" class="form-control login-input {{ $errors->has('title') ? ' is-invalid' : '' }}" required>
                                    </div>
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="message" class="control-label">{{__('facility.message')}} *</label>
                                        <input type="text" name="message" id="message" data-live-search="true" class="form-control login-input {{ $errors->has('message') ? ' is-invalid' : '' }}" required>
                                    </div>
                                    @if ($errors->has('message'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group label-floating">
                                        <label for="send" class="control-label mb-4"> </label>
                                        <input type="submit" name="send" id="send" class="btn btn-primary btn-block border-radius-20 bg-primary {{ $errors->has('message') ? ' is-invalid' : '' }}" value="{{__('facility.send')}}" required>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if(!empty($Object->attachment))
        <div class="modal fade" id="attachment" tabindex="-1" role="dialog" aria-labelledby="attachment" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-body">
                    <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        @foreach($Object->attachment as $img)
                                            <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" @if($loop->index == 0) class="active" @endif></li>
                                        @endforeach
                                    </ol>
                                    <div class="carousel-inner">
                                        @foreach($Object->attachment as $img)
                                            <div class="carousel-item @if($loop->index == 0) active @endif">
                                                <img src="{{asset((file_exists($img)?$img:'public/assets/img/cover.jpeg'))}}" class="d-block w-100" alt="...">
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    @if($Object->status == \App\Models\Orders\Order::Status['New'])
        <div class="modal fade" id="accept" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form action="{{url('facility/orders/'.$Object->id)}}" class="w-100" method="post">
                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <input type="hidden" name="status" value="{{ \App\Models\Orders\Order::Status['Accepted']}}">
                    <div class="modal-content text-center">
                        <div class="modal-body p-4">
                            <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label for="doctor_id" class="control-label">{{__('Doctor.the_doctor')}} *</label>
                                        <select name="doctor_id" id="doctor_id" data-live-search="true" class="form-control login-select {{ $errors->has('doctor_id') ? ' is-invalid' : '' }}" required>
                                            @foreach($Doctors as $Doctor)
                                                <option value="{{$Doctor->id}}" @if($Object->doctor_id == $Doctor->id) selected @endif>{{$Doctor->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @if ($errors->has('doctor_id'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('doctor_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label for="order_date" class="control-label">{{__('Order.order_date')}} *</label>
                                        <input type="date" id="order_date" name="order_date" required class="form-control login-input p-0 {{ $errors->has('order_date') ? ' is-invalid' : '' }}" value="{{$Object->order_date}}">
                                    </div>
                                    @if ($errors->has('order_date'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="order_time_from" class="control-label">{{__('Order.order_time_from')}} *</label>
                                        <input type="time" id="order_time_from" name="order_time_from" required class="form-control login-input p-0 {{ $errors->has('order_time_from') ? ' is-invalid' : '' }}" value="{{$Object->order_time_from}}">
                                    </div>
                                    @if ($errors->has('order_time_from'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_time_from') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="order_time_to" class="control-label">{{__('Order.order_time_to')}} *</label>
                                        <input type="time" id="order_time_to" name="order_time_to" required class="form-control login-input p-0 {{ $errors->has('order_time_to') ? ' is-invalid' : '' }}" value="{{$Object->order_time_to}}">
                                    </div>
                                    @if ($errors->has('order_time_to'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('order_time_to') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-success btn-block border-radius-20">{{__('facility.accept')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form action="{{url('facility/orders/'.$Object->id)}}" class="w-100" method="post">
                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <input type="hidden" name="status" value="{{ \App\Models\Orders\Order::Status['Rejected']}}">
                    <div class="modal-content text-center">
                        <div class="modal-body p-4">
                            <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label for="reject_reason" class="control-label">{{__('Order.reject_reason')}} </label>
                                        <textarea id="reject_reason" name="reject_reason" class="form-control login-input p-0 {{ $errors->has('reject_reason') ? ' is-invalid' : '' }}"></textarea>
                                    </div>
                                    @if ($errors->has('reject_reason'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('reject_reason') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-danger btn-block border-radius-20">{{__('facility.reject')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
    @if($Object->status == \App\Models\Orders\Order::Status['Accepted'])
        <div class="modal fade" id="finish" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form action="{{url('facility/orders/'.$Object->id)}}" class="w-100" method="post">
                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <input type="hidden" name="status" value="{{ \App\Models\Orders\Order::Status['Finished']}}">
                    <div class="modal-content text-center">
                        <div class="modal-body p-4">
                            <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>{{__('facility.sure_to_finish')}}</p>
                                </div>
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-dark border-radius-20 btn-block">{{__('facility.finish')}} {{__('Order.the_order')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="pharmacy_response" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <form action="{{url('facility/orders/'.$Object->id)}}" class="w-100" method="post">
                    <input name="_method" type="hidden" value="PATCH">
                    @csrf
                    <input type="hidden" name="order_state" value="{{ \App\Models\Orders\Order::OrderState['PharmacyReply']}}">
                    <div class="modal-content text-center">
                        <div class="modal-body p-4">
                            <a href="#" class="btn btn-danger border-radius-50 btn-sm modal-close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></a>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label for="delivery_method" class="control-label">{{__('Order.delivery_method')}} *</label>
                                        <select name="delivery_method" id="delivery_method" class="form-control login-select {{ $errors->has('delivery_method') ? ' is-invalid' : '' }}" required onchange="delivery_method_switch()">
                                            <option value="2">{{__('Order.delivery.true')}}</option>
                                            <option value="1">{{__('Order.delivery.false')}}</option>
                                        </select>
                                    </div>
                                    @if ($errors->has('delivery_method'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('delivery_method') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row" id="delivery_method_div">
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="delivery_duration" class="control-label">{{__('Order.delivery_duration')}} *</label>
                                        <input type="time" id="delivery_duration" name="delivery_duration" required class="form-control login-input p-0 {{ $errors->has('delivery_duration') ? ' is-invalid' : '' }}" value="{{$Object->delivery_duration}}">
                                    </div>
                                    @if ($errors->has('delivery_duration'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('delivery_duration') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group label-floating">
                                        <label for="delivery_cost" class="control-label">{{__('Order.delivery_cost')}} *</label>
                                        <input type="number" id="delivery_cost" name="delivery_cost" required class="form-control login-input p-0 {{ $errors->has('delivery_cost') ? ' is-invalid' : '' }}" value="{{$Object->delivery_cost}}">
                                    </div>
                                    @if ($errors->has('delivery_cost'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('delivery_cost') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label for="price" class="control-label">{{__('Order.price')}} *</label>
                                        <input type="number" id="price" name="price" required class="form-control login-input p-0 {{ $errors->has('price') ? ' is-invalid' : '' }}" value="{{$Object->price}}">
                                    </div>
                                    @if ($errors->has('price'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group label-floating">
                                        <label for="instructions" class="control-label">{{__('Order.instructions')}} *</label>
                                        <textarea id="instructions" name="instructions" required class="form-control login-input p-0 {{ $errors->has('instructions') ? ' is-invalid' : '' }}">{{$Object->instructions}}</textarea>
                                    </div>
                                    @if ($errors->has('instructions'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('instructions') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-warning border-radius-20 btn-block">{{__('facility.pharmacy_response')}}</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    @endif
@endsection
@section('script')
    <script>
        function delivery_method_switch(){
            let delivery_method_div = document.getElementById('delivery_method_div');
            let delivery_duration = document.getElementById('delivery_duration');
            let delivery_cost = document.getElementById('delivery_cost');
            let delivery_method = document.getElementById('delivery_method');
            let value = delivery_method.options[delivery_method.selectedIndex].value;
            if(value === '2'){
                delivery_duration.removeAttribute('disabled');
                delivery_cost.removeAttribute('disabled');
                delivery_duration.required = true;
                delivery_cost.required = true;
                delivery_method_div.style.display = "flex";
            }else{
                delivery_duration.disabled = true;
                delivery_cost.disabled = true;
                delivery_duration.removeAttribute('required');
                delivery_cost.removeAttribute('required');
                delivery_method_div.style.display = "none";
            }
        }
    </script>
@endsection
