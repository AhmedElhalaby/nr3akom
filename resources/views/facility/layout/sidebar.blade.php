<div class="user-img text-center h-25">
    <img src="{{asset((file_exists(auth()->user()->image))?auth()->user()->image:'public/users/image/default.jpg')}}" class="w-50" alt="">
</div>
<aside class="sidebar h-75">
    <ul>
        <li><a href="{{url('facility')}}" @if(url()->current() == url('facility')) class="active" @endif><i class="fa fa-home"></i> <span>{{__('facility.sidebar.home')}}</span></a></li>
        <li><a href="{{url('facility/doctors')}}" @if(strpos(url()->current() , url('facility/doctors'))===0) class="active" @endif><i class="fa fa-user-md"></i> <span>{{__('facility.sidebar.doctors')}}</span></a></li>
        <li><a href="{{url('facility/patients')}}" @if(strpos(url()->current() , url('facility/patients'))===0) class="active" @endif><i class="fa fa-users"></i> <span>{{__('facility.sidebar.patients')}}</span></a></li>
        <li><a href="{{url('facility/orders')}}" @if(strpos(url()->current() , url('facility/orders'))===0) class="active" @endif><i class="fa fa-clipboard-list"></i> <span>{{__('facility.sidebar.orders')}}</span></a></li>
        <li><a href="{{url('facility/profile')}}" @if(strpos(url()->current() , url('facility/profile'))===0) class="active" @endif><i class="fa fa-pen-square"></i> <span>{{__('facility.sidebar.edit_profile')}}</span></a></li>
        <li><a href=""><i class="fa fa-cogs"></i> <span>{{__('facility.sidebar.settings')}}</span></a></li>
    </ul>
</aside>
