<nav class="navbar navbar-expand-md bg-white h-100">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" href="{{url('app/lang')}}" data-toggle="tooltip" title="@if(app()->getLocale()=='ar'){{__('En')}}@else{{__('عربي')}}@endif"><i class="fa fa-language fa-rotate-180 color-gray" ></i></a>
        </li>
        <li class="nav-item">
            <form action="{{route('facility.logout')}}" method="post" class="hidden">
                @csrf
                <input type="submit" id="logout" class="hidden">
            </form>
            <a class="nav-link" href="#" onclick="document.getElementById('logout').click()" data-toggle="tooltip" title="{{__('auth.logout')}}"><i class="fa fa-sign-out-alt @if(app()->getLocale()=='ar') fa-rotate-180 @endif color-gray" ></i></a>
        </li>
    </ul>
    <form class="form-inline my-2 my-lg-0 hidden">
        <input class="form-control mr-sm-2 input" type="search" placeholder="{{__('facility.search')}}" aria-label="Search">
    </form>
</nav>
