<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}} {{@$title}}</title>
    {{--    *****************************    Plugins & Fonts    ********************************   --}}
    <link rel="stylesheet" href="{{asset('public/facility/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/facility/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/facility/css/custom.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo&display=swap">
    <link rel="stylesheet" href="{{asset('public/facility/fonts/fontawesome/css/all.css')}}">
    @if(app()->getLocale() == 'ar')
        <link rel="stylesheet" href="{{asset('public/facility/css/rtl.css')}}">
    @endif
    @yield('style')
</head>
<body>
<div class="container-fluid">
    <div class="row bg-light-gray">
        <div class="col-md-2 bg-primary sidebar-full vh-100 p-0">
            @include('facility.layout.sidebar')
        </div>
        <div class="col-md-10 p-0 vh-100">
            <div class="h-10">
                @include('facility.layout.navbar')
            </div>
            <div class="h-90 pt-4 overflow-auto">
                @yield('content')
            </div>
        </div>

    </div>
</div>
<script src="{{asset('public/facility/js/jquery.slim.js')}}"></script>
<script src="{{asset('public/facility/js/popper.min.js')}}"></script>
<script src="{{asset('public/facility/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/facility/js/bootstrap-select/bootstrap-select.min.js')}}"></script>
@if(app()->getLocale() == 'ar')
    <script src="{{asset('public/facility/js/bootstrap-select/defaults-ar_AR.js')}}"></script>
@endif
<script src="{{asset('public/facility/js/custom.js')}}"></script>
<script src="{{asset('public/facility/fonts/fontawesome/js/all.js')}}"></script>
<script src="{{asset('public/facility/js/moment.js')}}"></script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<!-- Insert these scripts at the bottom of the HTML, but before you use any Firebase services -->

<!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-app.js"></script>

<!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-analytics.js"></script>

<!-- Add Firebase products that you want to use -->
<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-auth.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-firestore.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.0.0/firebase-messaging.js"></script>


<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->

<script>
    // Your web app's Firebase configuration
    var firebaseConfig = {
        apiKey: "AIzaSyAfN88gZUovDn4bUpnMrO2ynUdTFOUSi2w",
        authDomain: "nrakum-5a3af.firebaseapp.com",
        databaseURL: "https://nrakum-5a3af.firebaseio.com",
        projectId: "nrakum-5a3af",
        storageBucket: "nrakum-5a3af.appspot.com",
        messagingSenderId: "313595193050",
        appId: "1:313595193050:web:9f43716f78d20e11c9e7e3"
    };

    // Initialize Firebase
    firebase.initializeApp(firebaseConfig);
    const messaging = firebase.messaging();
    messaging.usePublicVapidKey("BEfPEMCTOo9jiHtInRSaFrgllXX9jK_yWFxDfwm0u4_UiDOdqfkOpXQn_qSnp3vZDFzvZpY9G6jeesVWbYUQn3M");
    messaging.getToken().then((currentToken) => {
        if (currentToken) {
            sendTokenToServer(currentToken);
            updateUIForPushEnabled(currentToken);
        } else {
            // Show permission request.
            console.log('No Instance ID token available. Request permission to generate one.');
            // Show permission UI.
            updateUIForPushPermissionRequired();
            setTokenSentToServer(false);
        }
    }).catch((err) => {
        console.log('An error occurred while retrieving token. ', err);
        showToken('Error retrieving Instance ID token. ', err);
        setTokenSentToServer(false);
    });
    Notification.requestPermission().then(function(result) {
        if (result === 'denied') {
            console.log('Permission wasn\'t granted. Allow a retry.');
            return;
        }
        if (result === 'default') {
            console.log('The permission request was dismissed.');
            return;
        }
        // Do something with the granted permission.
    });
</script>
@yield('script')

</body>
</html>
