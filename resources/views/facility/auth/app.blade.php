<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{config('app.name')}} {{@$title}}</title>
    {{--    *****************************    Plugins & Fonts    ********************************   --}}
    <link rel="stylesheet" href="{{asset('public/facility/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/facility/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/facility/css/custom.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Cairo&display=swap">
    <link rel="stylesheet" href="{{asset('public/facility/fonts/fontawesome/css/all.css')}}">
    @if(app()->getLocale() == 'ar')
        <link rel="stylesheet" href="{{asset('public/facility/css/rtl.css')}}">
    @endif
</head>
<body>
    <div class="container-fluid ">
        <div class="row vh-100">
            @yield('content')
        </div>
    </div>
    <script src="{{asset('public/facility/js/jquery.slim.js')}}"></script>
    <script src="{{asset('public/facility/js/popper.min.js')}}"></script>
    <script src="{{asset('public/facility/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('public/facility/js/bootstrap-select/bootstrap-select.min.js')}}"></script>
    @if(app()->getLocale() == 'ar')
        <script src="{{asset('public/facility/js/bootstrap-select/defaults-ar_AR.js')}}"></script>
    @endif
    <script src="{{asset('public/facility/js/custom.js')}}"></script>
    <script src="{{asset('public/facility/fonts/fontawesome/js/all.js')}}"></script>
</body>
</html>
