@extends('facility.auth.app')
@section('content')
    <div class="col-md-6 pt-5 px-120">
        <div class="login-header">
            <a class="nav-link" href="{{url('app/lang')}}" data-toggle="tooltip" title="@if(app()->getLocale()=='ar'){{__('En')}}@else{{__('عربي')}}@endif"><i class="fa fa-language fa-rotate-180 color-gray" ></i></a>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="list-unstyled m-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('register')}}" method="post">
            @csrf
            <div class="form-group text-center">
                <label for="name">{{__('User.name')}}</label>
                <input type="text" name="name" id="name" class="form-control login-input" required placeholder="john smith‬‬">
            </div>
            <div class="form-group text-center">
                <label for="email">{{__('User.email')}}</label>
                <input type="email" name="email" id="email" class="form-control login-input" required placeholder="‫‪ex@ex.com‬‬">
            </div>
            <div class="form-group text-center">
                <label for="mobile">{{__('User.mobile')}}</label>
                <input type="tel" name="mobile" id="mobile" class="form-control login-input" required placeholder="‫‪00123456789‬‬">
            </div>
            <div class="form-group text-center">
                <label for="password">{{__('User.password')}}</label>
                <input type="password" name="password" id="password" class="form-control login-input" required placeholder="*******">
            </div>
            <div class="form-group text-center">
                <label for="password_confirmation">{{__('User.password_confirmation')}}</label>
                <input type="password" name="password_confirmation" id="password_confirmation" class="form-control login-input" required placeholder="*******">
            </div>
            <p class="text-center"> {{__('auth.privacy_click')}} <a href="" class="color-primary ">{{__('auth.privacy')}}</a></p>
            <div class="form-group text-center">
                <input type="submit" name="register" class="btn btn-block btn-primary login-btn" value="{{__('auth.register')}}">
            </div>
        </form>
    </div>
    @include('facility.auth.welcome')
@endsection
