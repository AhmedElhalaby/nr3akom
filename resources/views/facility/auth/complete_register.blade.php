@extends('facility.auth.app')
@section('content')
    <div class="col-md-6 pt-4 px-120">
        <div class="login-header">
            <a class="" href="#" onclick="document.getElementById('logout').click()" data-toggle="tooltip" title="{{__('auth.logout')}}"><i class="fa fa-sign-out-alt @if(app()->getLocale()=='en') fa-rotate-180 @endif color-gray" ></i></a>

            <a class="" href="{{url('app/lang')}}" data-toggle="tooltip" title="@if(app()->getLocale()=='ar'){{__('En')}}@else{{__('عربي')}}@endif"><i class="fa fa-language fa-rotate-180 color-gray" ></i></a>

        </div>
        <form action="{{url('facility/register/complete')}}" method="post" class="p-0" enctype="multipart/form-data">
            @csrf
            <div class="form-group text-center">
                <label for="name">{{__('Facility.name')}}</label>
                <input name="name" id="name" class="form-control login-input" value="{{old('name')}}" required>
            </div>

            <div class="form-group text-center">
                <label for="services">{{__('Facility.services')}}</label>
                <select id="services" class="form-control login-select" name="services[]" multiple required>
                    @foreach(App\Models\General\Service::all() as $service)
                        <optgroup label="{{$service->final_name}}">
                            @foreach($service->service_sections as $service_section)
                            <option value="{{$service_section->id}}">{{$service_section->final_name}}</option>
                            @endforeach
                        </optgroup>
                    @endforeach
                </select>
            </div>
            <div class="form-group text-center">
                <label for="facility_type_id">{{__('Facility.facility_type_id')}}</label>
                <select name="facility_type_id" id="facility_type_id" class="form-control login-select" required>
                    @foreach(App\Models\General\FacilityType::all() as $facility_type)
                        <option value="{{$facility_type->id}}" @if(old('facility_type_id') == $facility_type->id) selected @endif>{{$facility_type->final_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group text-center">
                <label for="supported_insurances">{{__('Facility.insurances')}}</label>
                <select id="supported_insurances" class="form-control login-select" name="supported_insurances[]" multiple required>
                    @foreach(App\Models\General\Insurance::all() as $insurance)
                        <option value="{{$insurance->id}}">{{$insurance->final_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group text-center">
                <label for="license_num">{{__('Facility.license_num')}}</label>
                <div class="input-group">
                    <div class="input-group-append">
                        <label class="input-group-text" onclick="document.getElementById('license_copy').click()">{{__('facility.upload_image')}}</label>
                    </div>
                    <input type="text" name="license_num" id="license_num" class="form-control login-input login-input2" required>
                    <input type="file" class="hidden" name="license_copy" id="license_copy">
                </div>
            </div>
            <div class="form-group text-center">
                <label for="commercial_register_num">{{__('Facility.commercial_register_num')}}</label>
                <div class="input-group">
                    <div class="input-group-append">
                        <label class="input-group-text" onclick="document.getElementById('commercial_register_copy').click()">{{__('facility.upload_image')}}</label>
                    </div>
                    <input type="text" name="commercial_register_num" id="commercial_register_num" class="form-control login-input login-input2" required>
                    <input type="file" class="hidden" name="commercial_register_copy" id="commercial_register_copy">
                </div>
            </div>
            <br>
            <div class="form-group">
                <input type="submit" name="submit" class="btn btn-block btn-primary login-btn" value="{{__('facility.save')}}">
            </div>
        </form>
    </div>
    @include('facility.auth.welcome')
@endsection
