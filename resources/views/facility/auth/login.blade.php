@extends('facility.auth.app')
@section('content')
    <div class="col-md-6 pt-165 px-120">
        <div class="login-header">
            <a class="nav-link" href="{{url('app/lang')}}" data-toggle="tooltip" title="@if(app()->getLocale()=='ar'){{__('En')}}@else{{__('عربي')}}@endif"><i class="fa fa-language fa-rotate-180 color-gray" ></i></a>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul class="list-unstyled m-0">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{url('login')}}" method="post">
            @csrf
            <div class="form-group text-center">
                <label for="email">{{__('User.email')}}</label>
                <input type="email" name="email" id="email" class="form-control login-input {{ $errors->has('email') ? ' is-invalid' : '' }}" required placeholder="‫‪ex@ex.com‬‬">
            </div>
            <div class="form-group text-center">
                <label for="password">{{__('User.password')}}</label>
                <input type="password" name="password" id="password" class="form-control login-input {{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="*******">
            </div>
            <div class="form-group text-center">
                <label for="login"></label>
                <input type="submit" name="login" id="login" class="btn btn-block btn-primary login-btn" value="{{__('auth.login')}}">
            </div>
            <br>
            <a href="{{url('register')}}" class="color-primary float-left ">{{__('auth.register_new_account')}}</a>
            <a href="" class="color-gray float-right">{{__('auth.forget_password')}}</a>
        </form>
    </div>
    @include('facility.auth.welcome')
@endsection
