<div class="col-md-6 bg-primary color-white text-center">
    <img src="{{asset('public/logo/logo.svg')}}" class="pt-135 w-25" alt="">
    <h4>{{__('facility.welcome')}}</h4>
    <p>@if(auth()->check()) {{__('facility.welcome_should_complete')}} @else {{__('facility.welcome_should_register')}} @endif</p>
</div>
