@extends('facility.layout.app')
@php
$logged = auth()->user();
$facility = $logged->facility;
@endphp

@section('content')
    <div class="container">
        <div class="row p-0 m-0">
            <div class="col-md-12">
                <div class="panel">
                    <div class="panel-header p-2">
                        <span>{{__('facility.sidebar.edit_profile')}}</span>
                    </div>
                    <div class="panel-body max-vh-75">
                        <div class="col-md-12 pt-4 px-120">
                            <form action="{{url('facility/profile')}}" method="post" class="p-0" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group text-center">
                                    <label for="name">{{__('Facility.name')}}</label>
                                    <input name="name" id="name" class="form-control login-input" value="{{$facility->name}}" required>
                                </div>

                                <div class="form-group text-center">
                                    <label for="services">{{__('Facility.services')}}</label>
                                    <select id="services" class="form-control login-select" name="services[]" multiple required>
                                        @foreach(App\Models\General\Service::all() as $service)
                                            <optgroup label="{{$service->final_name}}">
                                                @foreach($service->service_sections as $service_section)
                                                    <option value="{{$service_section->id}}"  @if(App\Models\Facilities\FacilityServices::where('facility_id',$facility->id)->where('service_section_id',$service_section->id)->first()) selected @endif>{{$service_section->final_name}}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group text-center">
                                    <label for="facility_type_id">{{__('Facility.facility_type_id')}}</label>
                                    <select name="facility_type_id" id="facility_type_id" class="form-control login-select" required>
                                        @foreach(App\Models\General\FacilityType::all() as $facility_type)
                                            <option value="{{$facility_type->id}}" @if($facility->facility_type_id == $facility_type->id) selected @endif>{{$facility_type->final_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group text-center">
                                    <label for="supported_insurances">{{__('Facility.insurances')}}</label>
                                    <select id="supported_insurances" class="form-control login-select" name="supported_insurances[]" multiple required>
                                        @foreach(App\Models\General\Insurance::all() as $insurance)
                                            <option value="{{$insurance->id}}" @if(App\Models\Facilities\FacilityInsurances::where('facility_id',$facility->id)->where('insurance_id',$insurance->id)->first()) selected @endif>{{$insurance->final_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group text-center">
                                    <label for="license_num">{{__('Facility.license_num')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <label class="input-group-text" onclick="document.getElementById('license_copy').click()">{{__('facility.upload_image')}}</label>
                                        </div>
                                        <input type="text" name="license_num" id="license_num" class="form-control login-input login-input2" value="{{$facility->license_num}}" required>
                                        <input type="file" class="hidden" name="license_copy" id="license_copy">
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <label for="commercial_register_num">{{__('Facility.commercial_register_num')}}</label>
                                    <div class="input-group">
                                        <div class="input-group-append">
                                            <label class="input-group-text" onclick="document.getElementById('commercial_register_copy').click()">{{__('facility.upload_image')}}</label>
                                        </div>
                                        <input type="text" name="commercial_register_num" id="commercial_register_num" class="form-control login-input login-input2" value="{{$facility->commercial_register_num}}" required>
                                        <input type="file" class="hidden" name="commercial_register_copy" id="commercial_register_copy">
                                    </div>
                                </div>
                                <br>
                                <div class="form-group">
                                    <input type="submit" name="submit" class="btn btn-block btn-primary login-btn" value="{{__('facility.save')}}">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
