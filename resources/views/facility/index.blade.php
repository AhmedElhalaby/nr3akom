@extends('facility.layout.app')
@section('content')
    <header class="row px-3 m-0">
        <div class="col-3 px-2">
            <div class="bg-success row m-1 header-card h-100 px-2" onclick="window.location='{{url('facility/orders')}}'">
                <div class="col-md-3 header-icon">
                    <i class="fa fa-clipboard-list"></i>
                </div>
                <div class="col-md-9 p-0 header-text">
                    <p class="card-header pt-4 pb-1 bg-transparent border-0">{{__('Order.orders')}}</p>
                    <p class="card-body pt-0">{{App\Models\Orders\Order::where('facility_id',auth()->user()->facility_id)->count()}}</p>
                </div>
            </div>
        </div>
        <div class="col-3 px-2">
            <div class="bg-info row m-1 header-card h-100 px-2" onclick="window.location='{{url('facility/doctors')}}'">
                <div class="col-md-3 header-icon">
                    <i class="fa fa-user-md"></i>
                </div>
                <div class="col-md-9 p-0 header-text">
                    <p class="card-header pt-4 pb-1 bg-transparent border-0">{{__('Doctor.doctors')}}</p>
                    <p class="card-body pt-0">{{App\Models\Auth\User::where('type',App\Models\Auth\User::TYPES['Doctor'])->where('facility_id',auth()->user()->facility_id)->count()}}</p>
                </div>
            </div>
        </div>
        <div class="col-3 px-2">
            <div class="bg-danger row m-1 header-card h-100 px-2" onclick="window.location='{{url('facility/profile')}}'">
                <div class="col-md-3 header-icon">
                    <i class="fa fa-check-square"></i>
                </div>
                <div class="col-md-9 p-0 header-text">
                    <p class="card-header pt-4 pb-1 bg-transparent border-0">{{__('Insurance.insurances')}}</p>
                    <p class="card-body pt-0">{{App\Models\Facilities\FacilityInsurances::where('facility_id',auth()->user()->facility_id)->count()}}</p>
                </div>
            </div>
        </div>
        <div class="col-3 px-2">
            <div class="bg-dark row m-1 header-card h-100 px-2" onclick="window.location='{{url('facility/profile')}}'">
                <div class="col-md-3 header-icon">
                    <i class="fa fa-server"></i>
                </div>
                <div class="col-md-9 p-0 header-text">
                    <p class="card-header pt-4 pb-1 bg-transparent border-0">{{__('Service.services')}}</p>
                    <p class="card-body pt-0">{{App\Models\Facilities\FacilityServices::where('facility_id',auth()->user()->facility_id)->count()}}</p>
                </div>
            </div>
        </div>
    </header>
    <div class="row p-3 m-0 mt-3">
        <div class="col-md-12 px-2">
            <div class="panel">
                <div class="panel-header p-2">
                    <span>{{__('facility.send_general_notification')}}</span>
                </div>
                <div class="panel-body max-vh-75">
                    <div class="container">
                        <form action="{{url('facility/notification')}}" class="row p-0 m-0 mt-4" method="post">
                            @csrf
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label for="title" class="control-label">{{__('facility.title')}} *</label>
                                    <input type="text" name="title" id="title" data-live-search="true" class="form-control login-input {{ $errors->has('title') ? ' is-invalid' : '' }}" required>
                                </div>
                                @if ($errors->has('title'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-5">
                                <div class="form-group label-floating">
                                    <label for="message" class="control-label">{{__('facility.message')}} *</label>
                                    <input type="text" name="message" id="message" data-live-search="true" class="form-control login-input {{ $errors->has('message') ? ' is-invalid' : '' }}" required>
                                </div>
                                @if ($errors->has('message'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('message') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <div class="form-group label-floating">
                                    <label for="type" class="control-label">{{__('facility.type')}} *</label>
                                    <select name="type" id="type" data-live-search="true" class="form-control bg-light-gray border-radius-20 px-3 bootstrap-select {{ $errors->has('type') ? ' is-invalid' : '' }}" required>
                                        <option value="0">{{__('facility.all')}}</option>
                                        <option value="1">{{__('Doctor.doctors')}}</option>
                                        <option value="2">{{__('Patient.patients')}}</option>
                                    </select>
                                </div>
                                @if ($errors->has('type'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-1">
                                <div class="form-group label-floating">
                                    <label for="send" class="control-label mb-4"> </label>
                                    <input type="submit" name="send" id="send" class="btn btn-primary btn-block border-radius-20 bg-primary {{ $errors->has('message') ? ' is-invalid' : '' }}" value="{{__('facility.send')}}" required>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row p-3 m-0">
        <section class="col-md-6 px-2">
            <div class="panel">
                <div class="panel-header p-2">
                    {{__('facility.Home.new_orders')}}
                </div>
                <div class="panel-body max-vh-50">
                    @foreach($NewOrders as $NewOrder)
                        <div class="row p-0 m-0 border-bottom item-hover" data-toggle="tooltip" title="{{__('facility.click_show_details')}}" onclick="window.location='{{url('facility/orders/'.$NewOrder->id)}}'">
                            <div class="col-md-9">
                                <div class="row p-0 m-0">
                                    <div class="col-md-4 p-3">
                                        <img src="{{asset((file_exists($NewOrder->user->image))?$NewOrder->user->image:'public/users/image/default.jpg')}}" class="border-radius-50" width="50" height="50" alt="">
                                    </div>
                                    <div class="col-md-8 p-1">
                                        <span class="d-block"><i class="fa fa-user float-left mt-1"></i> <span class="px-3">{{$NewOrder->user->name}}</span></span>
                                        <span class="d-block"><i class="fa fa-clipboard-list float-left mt-1"></i> <span class="px-3">{{$NewOrder->service_section->final_name .'-'.$NewOrder->service_section->service->final_name}}</span></span>
                                        <span class="d-block"><i class="fa fa-calendar float-left mt-1"></i> <span class="px-3">{{$NewOrder->order_date}}</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-center align-self-center">
                                <p class="badge status-bg-{{$NewOrder->status}}">{{__('Order.statuses.'.$NewOrder->status)}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
        <section class="col-md-6 px-2">
            <div class="panel">
                <div class="panel-header p-2">
                    {{__('facility.Home.today_orders')}}
                </div>
                <div class="panel-body max-vh-50">
                    @foreach($TodayOrders as $TodayOrder)
                        <div class="row p-0 m-0 border-bottom item-hover" data-toggle="tooltip" title="{{__('facility.click_show_details')}}" onclick="window.location='{{url('facility/orders/'.$TodayOrder->id)}}'">
                            <div class="col-md-9">
                                <div class="row p-0 m-0">
                                    <div class="col-md-4 p-3">
                                        <img src="{{asset((file_exists($TodayOrder->user->image))?$TodayOrder->user->image:'public/users/image/default.jpg')}}" class="border-radius-50" width="50" height="50" alt="">
                                    </div>
                                    <div class="col-md-8 p-1">
                                        <span class="d-block"><i class="fa fa-user"></i> <span class="px-3">{{$TodayOrder->user->name}}</span></span>
                                        <span class="d-block"><i class="fa fa-clipboard-list"></i> <span class="px-3">{{$TodayOrder->service_section->final_name .'-'.$TodayOrder->service_section->service->final_name}}</span></span>
                                        <span class="d-block"><i class="fa fa-calendar"></i> <span class="px-3">{{$TodayOrder->order_date}}</span></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 text-center align-self-center">
                                <p class="badge status-bg-{{$TodayOrder->status}}">{{__('Order.statuses.'.$TodayOrder->status)}}</p>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </section>
    </div>

@endsection
