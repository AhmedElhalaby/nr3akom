@extends('admin.app_settings.nationalities.main')
@php
    use  App\Models\General\Nationality;
    use  App\Models\General\Insurance;
@endphp
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.edit')}} {{__(($TheName))}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect.'/'.$Object->id)}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">{{__('User.name')}} *</label>
                                    <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$Object->name}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="email" class="control-label">{{__('User.email')}} *</label>
                                    <input type="email" id="email" name="email" required class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{$Object->email}}">
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="mobile" class="control-label">{{__('User.mobile')}} *</label>
                                    <input type="tel" id="mobile" name="mobile" required class="form-control {{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{$Object->mobile}}">
                                </div>
                                @if ($errors->has('mobile'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('mobile') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="gender" class="control-label">{{__('User.gender')}} *</label>
                                    <select id="gender" name="gender" required class="form-control {{ $errors->has('gender') ? ' is-invalid' : '' }}">
                                        <option value="1" @if($Object->gender == 1) selected @endif>{{__('User.Gender.male')}}</option>
                                        <option value="2" @if($Object->gender == 2) selected @endif>{{__('User.Gender.female')}}</option>
                                    </select>
                                </div>
                                @if ($errors->has('gender'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('gender') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="nationality_id" class="control-label">{{__('Patient.nationality_id')}} *</label>
                                    <select id="nationality_id" name="nationality_id" required class="form-control {{ $errors->has('nationality_id') ? ' is-invalid' : '' }}">
                                        @foreach(Nationality::all() as $item)
                                            <option value="{{$item->id}}" @if($Object->normal_user->nationality_id == $item->id) selected @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('nationality_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nationality_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="insurance_id" class="control-label">{{__('Patient.insurance_id')}} *</label>
                                    <select id="insurance_id" name="insurance_id" required class="form-control {{ $errors->has('insurance_id') ? ' is-invalid' : '' }}">
                                        @foreach(Insurance::all() as $item)
                                            <option value="{{$item->id}}" @if($Object->normal_user->insurance_id == $item->id) selected @endif>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('insurance_id'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('insurance_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <div class="form-group label-floating">
                                    <label for="dob" class="control-label">{{__('Patient.dob')}} *</label>
                                    <input type="date" id="dob" name="dob" required class="form-control {{ $errors->has('dob') ? ' is-invalid' : '' }}" value="{{$Object->normal_user->dob}}">
                                </div>
                                @if ($errors->has('dob'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('dob') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="address" class="control-label">{{__('Patient.address')}} *</label>
                                    <input type="text" id="address" name="address" required class="form-control {{ $errors->has('address') ? ' is-invalid' : '' }}" value="{{$Object->normal_user->address}}">
                                </div>
                                @if ($errors->has('address'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="identity" class="control-label">{{__('Patient.identity')}} *</label>
                                    <input type="text" id="identity" name="identity" required class="form-control {{ $errors->has('identity') ? ' is-invalid' : '' }}" value="{{$Object->normal_user->identity}}">
                                </div>
                                @if ($errors->has('identity'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('identity') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="medical_status" class="control-label">{{__('Patient.medical_status')}} *</label>
                                    <textarea id="medical_status" name="medical_status" required class="form-control {{ $errors->has('medical_status') ? ' is-invalid' : '' }}">{{$Object->normal_user->medical_status}}</textarea>
                                </div>
                                @if ($errors->has('medical_status'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('medical_status') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label for="identity_image" class="control-label">{{__('Patient.identity_image')}} *</label>
                                <input type="file" name="image" style="display: none" id="identity_image"  class="form-control" onchange="readURL(this,1);">
                                <img id="blah1" onclick="document.getElementById('identity_image').click()" src="@if($Object->normal_user && $Object->normal_user->identity_image) {{asset($Object->normal_user->identity_image)}} @else {{asset('public/assets/img/upload_identity.svg')}} @endif" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                            </div>
                            <div class="col-md-4">
                                <label for="insurance_image" class="control-label">{{__('Patient.insurance_image')}} *</label>
                                <input type="file" name="insurance_image" style="display: none" id="insurance_image"  class="form-control" onchange="readURL(this,2);">
                                <img id="blah2" onclick="document.getElementById('insurance_image').click()" src="@if($Object->normal_user && $Object->normal_user->insurance_image) {{asset($Object->normal_user->insurance_image)}} @else {{asset('public/assets/img/upload_insurance.svg')}} @endif" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                            </div>
                            <div class="col-md-4">
                                <label for="image" class="control-label">{{__('User.image')}}</label>
                                <input type="file" name="image" style="display: none" id="image"  class="form-control" onchange="readURL(this,3);">
                                <img id="blah3" onclick="document.getElementById('image').click()" src="@if($Object->image) {{asset($Object->image)}} @else {{asset('public/assets/img/upload_user.png')}} @endif" style="width: 120px;height: 120px" alt="upload image" class="thumbnail" />
                            </div>
                        </div>
                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script>
        function readURL(input,id) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah'+id)
                        .attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
