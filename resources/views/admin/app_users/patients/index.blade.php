@extends('admin.app_users.patients.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>
                        {{--{{__('Patient.image')}}--}}
                        </th>
                        <th>{{__('User.name')}} </th>
                        <th>{{__('User.email')}} </th>
                        <th>{{__('User.mobile')}} </th>
                        <th>{{__('User.gender')}} </th>
                        <th>{{__('Patient.nationality_id')}} </th>
                        <th>{{__('User.status')}} </th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                    <tr id="advance_search">
                        <form action="{{url($redirect)}}" >
                            <td></td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="name" class="hidden"></label>
                                    <input type="text" name="name" style="margin: 0;padding: 0" id="name" placeholder="{{__('User.name')}}" class="form-control" value="{{app('request')->input('name')}}">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="email" class="hidden"></label>
                                    <input type="text" name="email" style="margin: 0;padding: 0" id="email" placeholder="{{__('User.email')}}" class="form-control" value="{{app('request')->input('email')}}">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="mobile" class="hidden"></label>
                                    <input type="text" name="mobile" style="margin: 0;padding: 0" id="mobile" placeholder="{{__('User.mobile')}}" class="form-control" value="{{app('request')->input('mobile')}}">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="gender" class="hidden"></label>
                                    <select type="gender" name="gender" style="margin: 0;padding: 0" id="gender" class="form-control">
                                        <option value="" @if(app('request')->has('gender') && app('request')->input('gender') == '') selected @endif>-</option>
                                        <option value="{{app\Models\Auth\User::GENDER['Male']}}" @if(app('request')->has('gender') && app('request')->input('gender') == app\Models\Auth\User::GENDER['Male']) selected @endif>{{__('User.Gender.male')}}</option>
                                        <option value="{{app\Models\Auth\User::GENDER['Female']}}" @if(app('request')->has('gender') && app('request')->input('gender') == app\Models\Auth\User::GENDER['Female']) selected @endif>{{__('User.Gender.female')}}</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="nationality_id" class="hidden"></label>
                                    <select type="nationality_id" name="nationality_id" style="margin: 0;padding: 0" id="nationality_id" class="form-control">
                                        <option value="" @if(app('request')->has('nationality_id') && app('request')->input('nationality_id') == '') selected @endif>-</option>
                                        @foreach(\App\Models\General\Nationality::all() as $Nationality)
                                            <option value="{{$Nationality->id}}" @if(app('request')->has('nationality_id') && app('request')->input('nationality_id') == $Nationality->id) selected @endif>{{$Nationality->final_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="is_active" class="hidden"></label>
                                    <select type="is_active" name="is_active" style="margin: 0;padding: 0" id="is_active" class="form-control">
                                        <option value="" @if(app('request')->has('is_active') && app('request')->input('is_active') == '') selected @endif>-</option>
                                        <option value="1" @if(app('request')->has('is_active') && app('request')->input('is_active') == '1') selected @endif>{{__('User.active')}}</option>
                                        <option value="0" @if(app('request')->has('is_active') && app('request')->input('is_active') == '0') selected @endif>{{__('User.in_active')}}</option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                            </td>
                        </form>
                    </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td><img src="@if($item->image){{asset($item->image)}}@else {{asset('public/users/image/default.jpg')}} @endif" class=" img-thumbnail" alt="" style="width: 50px;height: 50px"></td>
                        <td>{{$item->name}}</td>
                        <td>{{$item->email}}</td>
                        <td>{{$item->mobile}}</td>
                        <td>{{$item->gender_str}}</td>
                        <td>{{($item->normal_user&&$item->normal_user->nationality)?$item->normal_user->nationality->name:'-'}}</td>
                        <td>@if($item->is_active) <span class="label label-success">{{__('User.active')}}</span>@else  <span class="label label-danger">{{__('User.in_active')}}</span> @endif</td>
                        <td class="text-primary">
                            <a href="{{url($redirect.'/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.edit')}}" class="fs-20"><i class="fa fa-edit"></i></a>
                            <a href="{{url($redirect.'/'.$item->id.'/activation')}}" data-toggle="tooltip" data-placement="bottom" title="@if($item->is_active) {{__('Admin.do_in_active')}} @else {{__('Admin.do_active')}} @endif" class="fs-20"><i class="fa @if($item->is_active) fa-window-close @else fa-check-square @endif"></i></a>
                            <a href="#" class="fs-20" data-toggle="modal" data-target="#EditPassword" onclick="document.getElementById('UserName').innerHTML = '{{$item->name}}';document.getElementById('user_id').value = '{{$item->id}}'"><i class="fa fa-key" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.change_password')}}"></i></a>
                            <a href="#" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$item->name}}';document.getElementById('id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.delete')}}"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name'=>request()->name,'email'=>request()->email,'gender'=>request()->gender,'nationality_id'=>request()->nationality_id,'mobile'=>request()->mobile,'is_active'=>request()->is_active])->links() }}
    </div>
</div>
@endsection

