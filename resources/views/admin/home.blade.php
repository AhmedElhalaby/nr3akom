@extends('admin.layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">  {{__('admin.Home.n_send_general')}} </h4>
                </div>
                <div class="card-content">
                    <form action="{{url('admin/notification/send')}}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-3 btn-group required">
                                <label for="title">{{__('admin.Home.n_title')}} :</label>
                                <input type="text" required="" name="title" id="title" class="form-control" placeholder="{{__('admin.Home.n_enter_title')}}">
                            </div>
                            <div class="col-md-5 btn-group required">
                                <label for="msg">{{__('admin.Home.n_text')}} :</label>
                                <input type="text" required="" name="msg" id="msg" class="form-control" placeholder="{{__('admin.Home.n_enter_text')}}">
                            </div>
                            <div class="col-md-2 btn-group required">
                                <label for="type">{{__('admin.Home.n_type')}} :</label>
                                <select required name="type" id="type" class="form-control">
                                    <option value="0">{{__('admin.Home.n_type_0')}}</option>
                                    <option value="1">{{__('admin.Home.n_type_1')}}</option>
                                    <option value="2">{{__('admin.Home.n_type_2')}}</option>
                                    <option value="3">{{__('admin.Home.n_type_3')}}</option>
                                </select>
                            </div>
                            <div class="col-md-1 " style="margin-top: 50px">
                                <button type="submit" id="send" class="btn btn-primary">{{__('admin.Home.n_send')}}</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
