<li class="nav-item @if(url()->current() == url('admin')) active @endif ">
    <a href="{{url('admin')}}" class="nav-link">
        <i class="material-icons">dashboard</i>
        <p>{{__('admin.sidebar.home')}}</p>
    </a>
</li>
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_managements" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_managements')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_managements'))===0) in @endif" id="app_managements" @if(strpos(url()->current() , url('admin/app_managements'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_managements/admins'))===0) active @endif">
                <a href="{{url('admin/app_managements/admins')}}" class="nav-link">
                    <i class="material-icons">group_add</i>
                    <p>{{__('admin.sidebar.admins')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>

<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_settings" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_settings')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_settings'))===0) in @endif" id="app_settings" @if(strpos(url()->current() , url('admin/app_settings'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/services'))===0) active @endif">
                <a href="{{url('admin/app_settings/services')}}" class="nav-link">
                    <i class="material-icons">view_quilt</i>
                    <p>{{__('admin.sidebar.services')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/facility_types'))===0) active @endif">
                <a href="{{url('admin/app_settings/facility_types')}}" class="nav-link">
                    <i class="material-icons">layers</i>
                    <p>{{__('admin.sidebar.facility_types')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/department_types'))===0) active @endif">
                <a href="{{url('admin/app_settings/department_types')}}" class="nav-link">
                    <i class="material-icons">style</i>
                    <p>{{__('admin.sidebar.department_types')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/specializations'))===0) active @endif">
                <a href="{{url('admin/app_settings/specializations')}}" class="nav-link">
                    <i class="material-icons">library_books</i>
                    <p>{{__('admin.sidebar.specializations')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/insurances'))===0) active @endif">
                <a href="{{url('admin/app_settings/insurances')}}" class="nav-link">
                    <i class="material-icons">file_copy</i>
                    <p>{{__('admin.sidebar.insurances')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_settings/nationalities'))===0) active @endif">
                <a href="{{url('admin/app_settings/nationalities')}}" class="nav-link">
                    <i class="material-icons">language</i>
                    <p>{{__('admin.sidebar.nationalities')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
<li class="nav-item ">
    <a class="nav-link collapsed" data-toggle="collapse" href="#app_users" aria-expanded="false">
        <i class="material-icons">keyboard_arrow_down</i>
        <p> {{__('admin.sidebar.app_users')}}</p>
    </a>
    <div class="collapse @if(strpos(url()->current() , url('admin/app_users'))===0) in @endif" id="app_users" @if(strpos(url()->current() , url('admin/app_users'))===0) aria-expanded="true" @endif>
        <ul class="nav">
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/patients'))===0) active @endif">
                <a href="{{url('admin/app_users/patients')}}" class="nav-link">
                    <i class="material-icons">supervisor_account</i>
                    <p>{{__('admin.sidebar.patients')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/facilities'))===0) active @endif">
                <a href="{{url('admin/app_users/facilities')}}" class="nav-link">
                    <i class="material-icons">apartment</i>
                    <p>{{__('admin.sidebar.facilities')}}</p>
                </a>
            </li>
            <li class="nav-item @if(strpos(url()->current() , url('admin/app_users/doctors'))===0) active @endif">
                <a href="{{url('admin/app_users/doctors')}}" class="nav-link">
                    <i class="material-icons">person</i>
                    <p>{{__('admin.sidebar.doctors')}}</p>
                </a>
            </li>
        </ul>
    </div>
</li>
