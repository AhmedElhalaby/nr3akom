<!doctype HTML>
<html lang="ar" data-color="{{ config('app.color') }}">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width,initial-scale=1" name="viewport">
    <link rel="stylesheet" href="{{asset('public/login/node_modules/bootstrap/dist/css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue-deep_purple.min.css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
    <link href="{{asset('public/login/fonts/fontello/css/fontello.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('public/login/css/bootstrap-offset-right.css')}}">
    <link rel="stylesheet" href="{{asset('public/login/css/style.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Cairo" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('public/assets/css/custom.css')}}">
    <title>{{config('app.name')}} - {{__('auth.login')}}</title>
    <style>
        .mdl-textfield__label {
            margin-bottom: 0;
            color: #d7dbdc;
            font-weight: normal;
        }

        .mdl-textfield--floating-label.is-focused .mdl-textfield__label,
        .mdl-textfield--floating-label.is-dirty .mdl-textfield__label {
            text-transform: uppercase
        }

        .has-feedback label~.form-control-feedback {
            top: 15px;
        }

        .mdl-textfield {
            width: 100%;
        }

        .mdl-checkbox__label {
            cursor: text;
            font-size: 13px;
            float: left;
            color: #b0b3b4;
            font-weight: normal;
        }

        .mdl-checkbox__box-outline {
            border: 1px solid #b0b3b4;
        }

        .mdl-textfield__input {
            border: none;
            border-bottom: 1px solid rgba(0, 0, 0, .12);
            display: block;
            font-size: 16px;
            margin: 0;
            padding: 4px 0;
            width: 100%;
            background: 0 0;
            text-align: left;
            color: inherit;
            font-weight: bold;
        }

        .mdl-switch__label {
            float: left;
            font-weight: normal;
            color: #b0b3b4;
            font-size: 14px;
        }
        .lt-register-btn:hover, .lt-register-btn:focus{
            background: #272262 !important;
        }
        span,strong,input{
            font-family: 'Cairo', sans-serif !important;
        }

        .mdl-textfield__label{
            top: 15px;
        }
        .mdl-textfield--floating-label.is-focused .mdl-textfield__label, .mdl-textfield--floating-label.is-dirty .mdl-textfield__label, .mdl-textfield--floating-label.has-placeholder .mdl-textfield__label{
            top: -5px !important;
        }
    </style>
</head>

<body>
<div class="container">
    <div class="center-block">
        <div class="col-md-3"></div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 no-padding">
            <div class="mlt-content">
                {{--<!--login form-->--}}
                <img src="{{asset('public/logo.png')}}" style="margin-top: 30px" width="200" height="150" alt="">

                @if (session('error'))
                    <div class="alert alert-danger" role="alert">
                        {{ session('error') }}
                    </div>
                @endif
                <form style="margin-top: 50px"  method="POST" action="{{ url('admin/login') }}">
                    @csrf
                    <div class="col-lg-10 col-lg-offset-1 col-lg-offset-right-1 col-md-10 col-md-offset-1 col-md-offset-right-1 col-sm-12 col-xs-12 pull-right ">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input autocomplete="email" class="mdl-textfield__input {{ $errors->has('email') ? ' is-invalid' : '' }}" style="text-align: center" type="email" id="email" name="email" required>
                            <label class="mdl-textfield__label" style="text-align: center" for="email">{{__('auth.email')}}</label>
                        </div>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-lg-10 col-lg-offset-1 col-lg-offset-right-1 col-md-10 col-md-offset-1 col-md-offset-right-1 col-sm-12 col-xs-12 pull-right ">
                        <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                            <input autocomplete="current-password" class="mdl-textfield__input {{ $errors->has('password') ? ' is-invalid' : '' }}" style="text-align: center" type="password" id="password" name="password" required>
                            <label class="mdl-textfield__label" style="text-align: center" for="password">{{__('auth.password')}}</label>
                        </div>
                        @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="col-lg-10 col-lg-offset-1 col-lg-offset-right-1 col-md-10 col-md-offset-1 col-md-offset-right-1 col-sm-12 col-xs-12 pull-right ">
                        <button class="btn lt-register-btn" type="submit">{{__('auth.login')}}  </button>
                    </div>
                </form>
                <!--login form-->
            </div>
            <!--Login-->
        </div>
        <div class="col-md-3"></div>
        <!--center-block-->
    </div>
    <!--container-->
</div>

<script src="{{asset('public/login/node_modules/jquery/dist/jquery.min.js')}}"></script>
<script src="{{asset('public/login/node_modules/bootstrap/dist/js/bootstrap.min.js')}}"></script>
<script src="{{asset('public/login/libs/mdl/material.min.js')}}"></script>

</body>

</html>
