@extends('admin.app_settings.service_sections.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.edit')}} {{__(($TheName))}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect.'/'.$Object->id)}}" method="post" enctype="multipart/form-data">
                        <input name="_method" type="hidden" value="PUT">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">{{__('ServiceSection.name')}} *</label>
                                    <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{$Object->name}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="ar_name" class="control-label">{{__('ServiceSection.ar_name')}} *</label>
                                    <input type="text" id="ar_name" name="ar_name" required class="form-control {{ $errors->has('ar_name') ? ' is-invalid' : '' }}" value="{{$Object->ar_name}}">
                                </div>
                                @if ($errors->has('ar_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ar_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="description" class="control-label">{{__('ServiceSection.description')}}</label>
                                    <textarea type="text" id="description" name="description" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}">{{$Object->description}}</textarea>
                                </div>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="ar_description" class="control-label">{{__('ServiceSection.ar_description')}}</label>
                                    <textarea type="text" id="ar_description" name="ar_description" class="form-control {{ $errors->has('ar_description') ? ' is-invalid' : '' }}">{{$Object->ar_description}}</textarea>
                                </div>
                                @if ($errors->has('ar_description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ar_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
