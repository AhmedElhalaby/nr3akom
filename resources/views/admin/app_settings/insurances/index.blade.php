@extends('admin.app_settings.insurances.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>{{__('Insurance.name')}} </th>
                        <th>{{__('Insurance.website')}}</th>
                        <th>{{__('Insurance.address')}}</th>
                        <th>{{__('Insurance.phone')}}</th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                        <tr id="advance_search">
                            <form action="{{url($redirect)}}" >
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="name" class="hidden"></label>
                                        <input type="text" name="name" style="margin: 0;padding: 0" id="name" placeholder="{{__('Insurance.name')}}" class="form-control" value="{{app('request')->input('name')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="website" class="hidden"></label>
                                        <input type="text" name="website" style="margin: 0;padding: 0" id="website" placeholder="{{__('Insurance.website')}}" class="form-control" value="{{app('request')->input('website')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="address" class="hidden"></label>
                                        <input type="text" name="address" style="margin: 0;padding: 0" id="address" placeholder="{{__('Insurance.address')}}" class="form-control" value="{{app('request')->input('address')}}">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group" style="margin:0;padding: 0 ">
                                        <label for="phone" class="hidden"></label>
                                        <input type="text" name="phone" style="margin: 0;padding: 0" id="phone" placeholder="{{__('Insurance.phone')}}" class="form-control" value="{{app('request')->input('phone')}}">
                                    </div>
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                                </td>
                            </form>
                        </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td>{{$item->name . ' - ' . $item->ar_name}}</td>
                        <td><a href="{{$item->website}}" target="_blank">{{$item->website}}</a></td>
                        <td>{{$item->address}}</td>
                        <td>{{$item->phone}} @if($item->phone2) - {{$item->phone2}} @endif</td>
                        <td class="text-primary">
                            <a href="{{url($redirect.'/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.edit')}}" class="fs-20"><i class="fa fa-edit"></i></a>
                            <a href="#" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$item->name}}';document.getElementById('id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.delete')}}"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'website' => request()->website,'address' => request()->address,'phone' => request()->phone])->links() }}
    </div>
</div>
@endsection

