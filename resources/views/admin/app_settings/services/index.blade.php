@extends('admin.app_settings.services.main')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header" data-background-color="{{ config('app.color') }}">
                <div class="row">
                    <div class="col-md-8">
                        <h4 class="title">{{__($Names)}}</h4>
                    </div>
                    <div class="col-md-4">
                        <div class="search" >
                            <form class="search form-group label-floating" action="{{url($redirect)}}">
                                <label for="q" class="control-label">{{__('admin.search')}} ...</label>
                                <input type="text" name="q" id="q" class="form-control" value="{{app('request')->input('q')}}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-content table-responsive">
                <table class="table">
                    <thead class="text-primary">
                        <th>{{__('Service.name')}} </th>
                        <th>{{__('Service.status')}} </th>
                        <th><a href="#" onclick="AdvanceSearch()">{{__('admin.advance_search')}} <i id="advance_search_caret" class="fa fa-caret-down"></i></a></th>
                    </thead>
                    <tbody>
                    <tr id="advance_search">
                        <form action="{{url($redirect)}}" >
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="name" class="hidden"></label>
                                    <input type="text" name="name" style="margin: 0;padding: 0" id="name" placeholder="{{__('Service.name')}}" class="form-control" value="{{app('request')->input('name')}}">
                                </div>
                            </td>
                            <td>
                                <div class="form-group" style="margin:0;padding: 0 ">
                                    <label for="active" class="hidden"></label>
                                    <select type="active" name="active" style="margin: 0;padding: 0" id="active" class="form-control">
                                        <option value=""@if(app('request')->has('active') && app('request')->input('active') == '') selected @endif>-</option>
                                        <option value="1" @if(app('request')->has('active') && app('request')->input('active') == '1') selected @endif>
                                            {{__('Service.active')}}
                                        </option>
                                        <option value="0" @if(app('request')->has('active') && app('request')->input('active') == '0') selected @endif>
                                            {{__('Service.in_active')}}
                                        </option>
                                    </select>
                                </div>
                            </td>
                            <td>
                                <input type="submit" class="btn btn-sm btn-primary" style="margin: 0;" value="{{__('admin.search')}}">
                            </td>
                        </form>
                    </tr>
                    @foreach($Objects as $item)
                    <tr>
                        <td>{{$item->name .' - '. $item->ar_name}}</td>
                        <td>@if($item->is_active) <span class="label label-success">{{__('Service.active')}}</span>@else  <span class="label label-danger">{{__('Service.in_active')}}</span> @endif</td>
                        <td class="text-primary">
                            <a href="{{url($redirect.'/'.$item->id.'/edit')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.edit')}}" class="fs-20"><i class="fa fa-edit"></i></a>
                            <a href="{{url($redirect.'/'.$item->id.'/service_sections')}}" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.sidebar.service_sections')}}" class="fs-20"><i class="fa fa-bars"></i></a>
                            <a href="{{url($redirect.'/'.$item->id.'/activation')}}" data-toggle="tooltip" data-placement="bottom" title="@if($item->is_active) {{__('Service.do_in_active')}} @else {{__('Service.do_active')}} @endif" class="fs-20"><i class="fa @if($item->is_active) fa-window-close @else fa-check-square @endif"></i></a>
                            <a href="#" class="fs-20" data-toggle="modal" data-target="#delete" onclick="document.getElementById('del_name').innerHTML = '{{$item->name}}';document.getElementById('id').value = '{{$item->id}}'"><i class="fa fa-trash" data-toggle="tooltip" data-placement="bottom" title="{{__('admin.delete')}}"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pagination-div">
        {{ $Objects->appends(['q' => request()->q,'name' => request()->name,'active' => request()->active])->links() }}
    </div>
</div>
@endsection

