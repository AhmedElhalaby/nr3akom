@extends('admin.app_settings.specializations.main')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header" data-background-color="{{ config('app.color') }}">
                    <h4 class="title">{{__('admin.add')}} {{__($Name)}}</h4>
                </div>
                <div class="card-content">
                    <form action="{{url($redirect)}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="name" class="control-label">{{__('Specialization.name')}} *</label>
                                    <input type="text" id="name" name="name" required class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{old('name')}}">
                                </div>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group label-floating">
                                    <label for="ar_name" class="control-label">{{__('Specialization.ar_name')}} *</label>
                                    <input type="text" id="ar_name" name="ar_name" required class="form-control {{ $errors->has('ar_name') ? ' is-invalid' : '' }}" value="{{old('ar_name')}}">
                                </div>
                                @if ($errors->has('ar_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ar_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="description" class="control-label">{{__('Specialization.description')}}</label>
                                    <textarea type="text" id="description" name="description" class="form-control {{ $errors->has('description') ? ' is-invalid' : '' }}">{{old('description')}}</textarea>
                                </div>
                                @if ($errors->has('description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="ar_description" class="control-label">{{__('Specialization.ar_description')}}</label>
                                    <textarea type="text" id="ar_description" name="ar_description" class="form-control {{ $errors->has('ar_description') ? ' is-invalid' : '' }}">{{old('ar_description')}}</textarea>
                                </div>
                                @if ($errors->has('ar_description'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('ar_description') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label for="services" class="control-label">{{__('Specialization.services')}}</label>
                                    <select type="text" id="services" multiple aria-multiselectable="true" name="services[]" class="form-control {{ $errors->has('services') ? ' is-invalid' : '' }}">
                                        @foreach(\App\Models\General\Service::all() as $service)
                                            <optgroup label="{{$service->final_name}}">
                                                @foreach($service->service_sections as $section)
                                                    <option value="{{$section->id}}">{{$section->final_name}}</option>
                                                @endforeach
                                            </optgroup>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('services'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('services') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="row submit-btn">
                            <button type="submit" class="btn btn-primary" style="margin-left:15px;margin-right:15px;">{{__('admin.save')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

