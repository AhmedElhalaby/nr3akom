<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'sidebar'=>[
        'home'=>'الرئيسية',
        'doctors'=>'الأطباء',
        'orders'=>'الطلبات',
        'edit_profile'=>'تعديل البيانات',
        'settings'=>'الإعدادات',
        'patients'=>'المرضى',
    ],
    'Home'=>[
        'new_orders'=>'أحدث الطلبات',
        'today_orders'=>"مواعيد اليوم",
    ],
/////////////////////// الأكثر استخداما ////////////////////////
    'print' => 'طباعة',
    'pdf' => 'بي دي اف',
    'excel' => 'إكسيل',
    'sure_to_delete' => 'هل انت متأكد انك تريد حذف ',
    'sure_to_finish' => 'هل انت متأكد انك تريد إنهاء ',
    'cancel' => 'إلغاء',
    'save' => 'حفظ',
    'close' => 'إغلاق',
    'export' => 'تصدير',
    'add' => 'أضف',
    'change_password' => 'تعديل كلمة المرور',
    'delete' => 'حذف',
    'edit' => 'تعديل',
    'search' => 'بحث',
    'advance_search' => 'بحث متقدم',
    'switch_lang' => 'تبديل اللغة',
    'send_general_notification' => 'ارسال إشعار عام',
    'send_notification' => 'ارسال إشعار',
    'exported_by' => 'تم التصدير بواسطة',
    'exported_date' => 'تاريخ التصدير',
    'click_show_details' => 'اضغط لعرض التفاصيل',
    'upload_image' => 'رفع صورة',
    'welcome'=>'أهلا وسهلا بكم في تطبيق الرعاية الصحية',
    'welcome_should_register'=>'يتوجب عليك تسجيل الدخول',
    'welcome_should_complete'=>'يرجى اكمال البيانات للتمتع بخدمات التطبيق',
    'accept'=>'قبول',
    'reject'=>'رفض',
    'finish'=>'انهاء',
    'pharmacy_response'=>'الرد على الطلب',
    'none'=>'لا يوجد',
    'title'=>'العنوان',
    'message'=>'النص',
    'send'=>'ارسال',
    'type'=>'النوع',
    'all'=>'الكل',
/////////////////////// رسائل اللوحة ////////////////////////
    'messages'=>[
        'deleted_successfully'=>'تم الحذف بنجاح !',
        'saved_successfully'=>'تم الحفظ بنجاح !',
        'wrong_data'=>'الإدخال خاطئ !',
        'your_account_is_in_active'=>'حسابك معطل !',
        'you_cant_deactivate_your_account'=>'لا يمكنك تعطيل حسابك !',
        'notification_sent'=>'تم ارسال الاشعار !',
    ],
];
