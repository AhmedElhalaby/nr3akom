<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */


    'doctors' => 'الأطباء',
    'doctor' => 'طبيب',
    'the_doctor' => 'الطبيب',

    'nationality_id'=>'الجنسية',
    'specialization_id'=>'التخصص',
    'birthday'=>'تاريخ الميلاد',
    'address'=>'العنوان',
    'national_address'=>'العنوان الوطني',
    'identity_image'=>'صورة الهوية',
    'identity_num'=>'رقم الهوية',
    'details'=>'التفاصيل',

];
