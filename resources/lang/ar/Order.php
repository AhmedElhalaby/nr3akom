<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'orders' => 'الطلبات',
    'order' => 'طلب',
    'the_order' => 'الطلب',
    'order_details'=>'تفاصيل الطلب',
    'patient_name'=>'اسم المريض',
    'service'=>'الخدمة',
    'order_date'=>'تاريخ الطلب',
    'price'=>'السعر',
    'status'=>'الحالة',
    'description'=>'الوصف',
    'attachments'=>'المرفقات',
    'no_attachments'=>'لا يوجد مرفقات',
    'order_time'=>'موعد الطلب',
    'order_time_from'=>'موعد الطلب من',
    'order_time_to'=>'موعد الطلب الى',
    'reject_reason'=>'سبب الرفض',
    'pharmacy_response'=>'بإنتظار رد الصيدلية',
    'delivery_method'=>'امكانية التوصيل',
    'delivery_cost'=>'تكلفة التوصيل',
    'delivery_duration'=>'وقت التوصيل',
    'instructions'=>'تعليمات الاستخدام',

    'statuses'=>[
        '0'=>'طلب جديد',
        '1'=>'تم قبول الطلب',
        '2'=>'تم إنهاء الطلب',
        '3'=>'تم رفض الطلب',
        '4'=>'طلب ملغي من قبل المريض',
    ],
    'delivery'=>[
        'true'=>'يوجد',
        'false'=>'لا يوجد',
    ],

];
