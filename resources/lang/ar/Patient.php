<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'patients' => 'المرضى',
    'patient' => 'مريض',
    'the_patient' => 'المريض',

    'nationality_id'=>'الجنسية',
    'insurance_id'=>'التأمين',
    'dob'=>'تاريخ الميلاد',
    'address'=>'العنوان',
    'identity'=>'رقم الهوية',
    'medical_status'=>'الحالة الصحية',
    'identity_image'=>'صورة الهوية',
    'insurance_image'=>'صورة التأمين',
];
