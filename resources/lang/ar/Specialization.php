<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'specializations' => 'التخصصات',
    'specialization' => 'تخصص',
    'the_specialization' => 'التخصص',
    'name' => 'الاسم',
    'ar_name' => 'الاسم بالعربية',
    'description' => 'الوصف',
    'ar_description' => 'الوصف بالعربية',
    'services' => 'الخدمات',

];
