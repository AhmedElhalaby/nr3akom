<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'facility_types' => 'أنواع المنشآت',
    'facility_type' => 'نوع منشأة',
    'the_facility_type' => 'نوع المنشأة',
    'name' => 'الاسم',
    'ar_name' => 'الاسم بالعربية',
    'description' => 'الوصف',
    'ar_description' => 'الوصف بالعربية',

];
