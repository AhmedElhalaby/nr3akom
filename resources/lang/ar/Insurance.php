<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'insurances' => 'شركات التأمين',
    'insurance' => 'شركة تأمين',
    'the_insurance' => 'شركة التأمين',
    'name' => 'الاسم',
    'ar_name' => 'الاسم بالعربية',
    'website' => 'الموقع الالكتروني',
    'address' => 'العنوان',
    'phone' => 'رقم الجوال',
    'phone2' => 'رقم الجوال 2',

];
