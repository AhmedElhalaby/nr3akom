<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'nationalityies' => 'الجنسيات',
    'nationality' => 'جنسية',
    'the_nationality' => 'الجنسية',
    'name' => 'الاسم',
    'ar_name' => 'الاسم بالعربية',
];
