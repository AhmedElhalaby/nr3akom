<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'admins' => 'المدراء',
    'admin' => 'مدير',
    'the_admin' => 'المدير',
    'name' => 'الاسم',
    'email' => 'البريد الالكتروني',
    'status' => 'الحالة',
    'password' => 'كلمة المرور',
    'password_confirmation' => 'تأكيد كلمة المرور',
    'active' => 'مفعل',
    'do_active' => 'تفعيل',
    'in_active' => 'معطل',
    'do_in_active' => 'تعطيل',

];
