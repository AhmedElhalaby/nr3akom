<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'specializations' => 'Specializations',
    'specialization' => 'Specialization',
    'the_specialization' => 'The Specialization',
    'name' => 'Name',
    'ar_name' => 'Name Ar',
    'description' => 'Description',
    'ar_description' => 'Description Ar',
    'services' => 'Services',

];
