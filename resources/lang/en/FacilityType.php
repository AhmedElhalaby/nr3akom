<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'facility_types' => 'Facility Types',
    'facility_type' => 'Facility Type',
    'the_facility_type' => 'The Facility Type',
    'name' => 'Name',
    'ar_name' => 'Name Ar',
    'description' => 'Description',
    'ar_description' => 'Description Ar',

];
