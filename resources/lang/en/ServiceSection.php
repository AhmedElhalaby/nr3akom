<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'service_sections' => 'Service Sections',
    'service_section' => 'Service Section',
    'the_service_section' => 'The Service Section',
    'name' => 'Name',
    'ar_name' => 'Name Ar',
    'description' => 'Description',
    'ar_description' => 'Description Ar',
    'status' => 'Status',
    'active' => 'Active',
    'do_active' => 'Activate',
    'in_active' => 'In-Active',
    'do_in_active' => 'De-Activate',
];
