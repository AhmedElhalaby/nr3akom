<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'facilities' => 'Facilities',
    'facility' => 'Facility',
    'the_facility' => 'The Facility',

    'name'=>'Facility Name',
    'services'=>'Services',
    'facility_type_id'=>'Facility Type',
    'insurances'=>'Insurances',
    'license_num'=>'License Num',
    'commercial_register_num'=>'Commercial Register Num',


];
