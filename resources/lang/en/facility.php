<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'sidebar'=>[
        'home'=>'Home',
        'doctors'=>'Doctors',
        'orders'=>'Orders',
        'edit_profile'=>'Edit Profile',
        'settings'=>'Settings',
        'patients'=>'Patients',
    ],
    'Home'=>[
        'new_orders'=>'New Orders',
        'today_orders'=>"Today's Orders",
    ],
/////////////////////// Most Used ////////////////////////
    'print' => 'Print',
    'pdf' => 'PDF',
    'excel' => 'Excel',
    'sure_to_delete' => 'Are You Sure You Want To Delete',
    'sure_to_finish' => 'Are You Sure You Want To Finish',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'close' => 'Close',
    'export' => 'Export',
    'add' => 'Add',
    'change_password' => 'Edit Password',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'search' => 'Search',
    'advance_search' => 'Advance Search',
    'switch_lang' => 'Switch Language',
    'exported_by' => 'Exported By',
    'exported_date' => 'Exported Date',
    'click_show_details' => 'Click to view details',
    'upload_image' => 'Upload',
    'welcome'=>'Welcome to WeCare App',
    'welcome_should_register'=>'You have to register',
    'welcome_should_complete'=>'You should complete your registration',
    'accept'=>'Accept',
    'reject'=>'Reject',
    'finish'=>'End',
    'none'=>'None',
    'send_general_notification' => 'Send General Notification',
    'send_notification' => 'Send Notification',
    'pharmacy_response'=>'Response on the Order',
    'title'=>'Title',
    'message'=>'Message',
    'send'=>'Send',
    'type'=>'Type',
    'all'=>'All',

/////////////////////// Response Messages ////////////////////////
    'messages'=>[
        'deleted_successfully'=>'Deleted Successfully !',
        'saved_successfully'=>'Saved Successfully !',
        'wrong_data'=>'Wrong Data !',
        'your_account_is_in_active'=>'Your Account is in-active !',
        'you_cant_deactivate_your_account'=>'You Can`nt de-activate your account  !',
        'notification_sent'=>'Notification Sent Successfully !',
    ],
];
