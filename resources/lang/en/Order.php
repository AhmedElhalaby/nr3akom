<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'orders' => 'Orders',
    'order' => 'Order',
    'the_order' => 'The Order',
    'order_details'=>'Order Details',
    'patient_name'=>'Patient Name',
    'service'=>'Service',
    'order_date'=>'Order Date',
    'price'=>'Price',
    'status'=>'Status',
    'description'=>'Description',
    'attachments'=>'Attachments',
    'no_attachments'=>'No Attachments',
    'order_time_from'=>'Order Time From',
    'order_time'=>'Order Time',
    'order_time_to'=>'Order Time To',
    'reject_reason'=>'Reject Reason',
    'pharmacy_response'=>'Wait Pharmacy Response',
    'delivery_method'=>'Delivery Method',
    'delivery_cost'=>'Delivery Cost',
    'delivery_duration'=>'Delivery Time',
    'instructions'=>'Use Instructions',

    'statuses'=>[
        '0'=>'New Order',
        '1'=>'Accepted Order',
        '2'=>'Finished Order',
        '3'=>'Rejected Order',
        '4'=>'Canceled Order From Patient',
    ],
    'delivery'=>[
        'true'=>'Available',
        'false'=>'Not Available',
    ],

];
