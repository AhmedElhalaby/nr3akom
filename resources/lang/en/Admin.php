<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'admins' => 'Admins',
    'admin' => 'Admin',
    'the_admin' => 'The Admin',
    'name' => 'Name',
    'email' => 'E-Mail',
    'status' => 'Status',
    'password' => 'Password',
    'password_confirmation' => 'Password Confirmation',
    'active' => 'Active',
    'do_active' => 'Activate',
    'in_active' => 'In-Active',
    'do_in_active' => 'De-Activate',

];
