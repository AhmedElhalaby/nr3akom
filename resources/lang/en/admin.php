<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'sidebar'=>[
        'home'=>'Home',
        'app_managements'=>'App Managements',
        'admins'=>'Admins',
        'app_settings'=>'App Settings',
        'facility_types'=>'Facility Types',
        'department_types'=>'Department Types',
        'specializations'=>'Specializations',
        'insurances'=>'Insurances',
        'nationalities'=>'Nationalities',
        'services'=>'Services',
        'service_sections'=>'Service Sections',
        'app_users'=>'App Users',
        'patients'=>'Patients',
        'facilities'=>'Facilities',
        'doctors'=>'Doctors',
    ],
    'Home'=>[
        'n_send_general'=>'Send Public Notification ',
        'n_title'=>'Notification Title',
        'n_text'=>'Notification Text',
        'n_enter_title'=>'Enter Notification Title',
        'n_enter_text'=>'Enter Notification Text',
        'n_send'=>'Send',
        'n_type'=>'Object',
        'n_type_0'=>'All',
        'n_type_1'=>'Facility',
        'n_type_2'=>'Doctor',
        'n_type_3'=>'User',
    ],
/////////////////////// Most Used ////////////////////////
    'print' => 'Print',
    'pdf' => 'PDF',
    'excel' => 'Excel',
    'sure_to_delete' => 'Are You Sure You Want To Delete',
    'cancel' => 'Cancel',
    'save' => 'Save',
    'close' => 'Close',
    'export' => 'Export',
    'add' => 'Add',
    'change_password' => 'Edit Password',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'search' => 'Search',
    'advance_search' => 'Advance Search',
    'switch_lang' => 'Switch Language',
    'exported_by' => 'Exported By',
    'exported_date' => 'Exported Date',

/////////////////////// Response Messages ////////////////////////
    'messages'=>[
        'deleted_successfully'=>'Deleted Successfully !',
        'saved_successfully'=>'Saved Successfully !',
        'wrong_data'=>'Wrong Data !',
        'your_account_is_in_active'=>'Your Account is in-active !',
        'you_cant_deactivate_your_account'=>'You Can`nt de-activate your account  !',
        'notification_sent'=>'Notification Sent Successfully !',

    ],
];
