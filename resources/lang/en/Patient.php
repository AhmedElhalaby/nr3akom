<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'patients' => 'Patients',
    'patient' => 'Patient',
    'the_patient' => 'The Patient',

    'nationality_id'=>'Nationality',
    'insurance_id'=>'Insurance',
    'dob'=>'Date Of Birth',
    'address'=>'Address',
    'identity'=>'Identity',
    'medical_status'=>'Medical Status',
    'identity_image'=>'Identity Image',
    'insurance_image'=>'Insurance Image',

];
