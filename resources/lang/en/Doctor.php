<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'doctors' => 'Doctors',
    'doctor' => 'Doctor',
    'the_doctor' => 'The Doctor',

    'nationality_id'=>'Nationality',
    'specialization_id'=>'Specialization',
    'birthday'=>'Date Of Birth',
    'address'=>'Address',
    'national_address'=>'National Address',
    'identity_image'=>'Identity Image',
    'identity_num'=>'Identity',
    'details'=>'Details',
];
