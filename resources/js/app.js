/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import './bootstrap';
import Vue from 'vue';
import Vuetify, { VLayout } from 'vuetify/lib'
import Routes from '@/js/routes.js';
import App from '@/js/views/App';

Vue.use(Vuetify, {
    components: { VLayout },
})
const app = new Vue({
    el: '#app',
    router:Routes,
    components:{App},
    render: h => h(App),
});

export default app;
